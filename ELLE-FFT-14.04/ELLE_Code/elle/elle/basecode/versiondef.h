 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile: versiondef.h,v $
 * Revision:  $Revision: 1.14 $
 * Date:      $Date: 2014/06/16 23:34:41 $
 * Author:    $Author: levans $
 *
 ******************************************************/
#ifndef _E_versiondef_h
#define _E_versiondef_h
const char *Version_num   =  "2.7";
const char *Patch_level =  ".0";

#endif
