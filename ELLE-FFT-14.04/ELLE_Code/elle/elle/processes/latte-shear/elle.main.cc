
/*
 *  main.cc
 */

#include <cstdio>
#include <cstdlib>

#include "error.h"
#include "parseopts.h"
#include "init.h"
#include "runopts.h"
#include "file.h"
#include "setup.h"
#include "experiment.h"

Experiment *MyExperiment;


main(int argc, char **argv)
{
    int err=0;
	
    //extern int Init_Experiment(void);
	
	MyExperiment = new Experiment;

    //*-----------------------------------------
    //* initialise
    //*-----------------------------------------

    ElleInit();
	

    //*--------------------------------------------------
    //* set the function to the one in your process file
    //*--------------------------------------------------

    //ElleSetInitFunction(InitSetMike);
	
	//ElleSetInitFunction(MyExperiment->Init_Experiment);
	
	ElleSetInitFunction(Init_Experiment);

    if (err=ParseOptions(argc,argv))
        OnError("",err);

	
    //*-------------------------------------------------------
    //* set the base for naming statistics and elle files
    //*------------------------------------------------------

    ElleSetSaveFileRoot("my_experiment");

	
    //*-------------------------------------
    //* set up the X window
    //*-------------------------------------

    if (ElleDisplay()) SetupApp(argc,argv);

    //*--------------------------------------------------------------
    //* run your initialisation function and start the application
    //*--------------------------------------------------------------
	
	

    StartApp();
	
	
    CleanUp();

    return(0);
} 
extern int Init_Experiment(void)
{
	
		//-----------------------------------------------------
	  // Set the run function
	  //-----------------------------------------------------

		 ElleSetRunFunction(Run_Experiment);
		MyExperiment->Init();
}
extern int Run_Experiment()
	{
		MyExperiment->Run();
	}
