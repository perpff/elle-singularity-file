#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <float.h>  
#include "attrib.h"
#include "nodes.h"
#include "update.h"
#include "error.h"
#include "parseopts.h"
#include "runopts.h"
#include "file.h"
#include "interface.h"
#include "init.h"
#include "setup.h"
#include "triattrib.h"
#include "unodes.h"
#include "polygon.h"
                      
using std::ios;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::string;
using std::vector;
using std::cin;

// funcions definides
int Input_tex(), Init_input();
int out_data();
void Jacobi_Cyclic_Method(double eigenvalues[3], double* eigenvectors, double *A, int n);
int LoadDataTemp(const char *fname);

	double Alocal[3][3],eigenvalues[3], eigenvectors[3][3]; 
	double A[3][3],B[3][3],C[3][3],D[3][3]; 
	double dd[1000][6]; // matrix to store the stress from all.out files
	int size;
	
 #define DTOR 3.1415926/180
main(int argc, char **argv)
{
    int err=0;
    UserData udata;

    /*
     * initialise
     */
    ElleInit();

    /*
     * set the function to the one in your process file
     */
    ElleSetInitFunction(Init_input);
	
	ElleUserData(udata);	
    ElleSetUserData(udata);

    if (err=ParseOptions(argc,argv))
        OnError("",err);
    ElleSetSaveFrequency(1);

    /*
     * set up the X window
     */
    if (ElleDisplay()) SetupApp(argc,argv);

    /*
     * set the base for naming statistics and elle files
     */
    // ElleSetSaveFileRoot("elle2IPF");

    /*
     * run your initialisation function and start the application
     */
    StartApp();

    CleanUp();

    return(0);
} 


/*
 * this function will be run when the application starts,
 * when an elle file is opened or
 * if the user chooses the "Rerun" option
 */
int Init_input()   //InitMoveBnodes()
{
    char *infile;
    int err=0;
    
    /*
     * clear the data structures
     */
    ElleReinit();

    ElleSetRunFunction(Input_tex);

    /*
     * read the data
     */
    infile = ElleFile();
    if (strlen(infile)>0) {
        if (err=ElleReadData(infile)) OnError(infile,err);
        /*
         * check for any necessary attributes which may
         * not have been in the elle file
         */
      //  if (!ElleUnodeAttributeActive(U_ATTRIB_A))
        //    ElleInitUnodeAttribute(U_ATTRIB_A);
	//	if (!ElleUnodeAttributeActive(U_ATTRIB_B))
      //      ElleInitUnodeAttribute(U_ATTRIB_B);
    }
}

/*!
 */


int Input_tex()
{
    int err=0, i;

    /*
     * set the x,y  to the values from the fft step
     */

    /*
     * set the euler angles to the values from the fft step
     */

    err = out_data(); 
    if (err) OnError("error",err);

    /*
     * write the updated Elle file
     */
    // if (err=ElleWriteData("analyzer_out.elle"))
       //  OnError("",err);
}

int out_data()
{
	int err=0;
    int i,j, k, max_unodes, n;
	double val[6], grainid, mean, tmp;
    double xyz[3][3];

	double phi1, Phi, phi2;

	double IA,IIA,IIIA;	
	UserData udata;
	ElleUserData(udata);
	 
		
    max_unodes = ElleMaxUnodes(); 
/*	
		for( k=0 ; k<max_unodes ;k++) 
		{

// Average misorientation (or the data stored in U_ATTRIB_C)				
			ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_C);	
			val[1] += val[0];
			
// Average dislocdensity 				
			ElleGetUnodeAttribute(k,&val[0],U_DISLOCDEN);	
			val[2] += val[0];

// Average stress
			ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_B);	
			val[5] += val[0];
			
// adimensional strain localisation (from Schueller, 2004)
			ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_A);	
			val[3] += val[0];
			
			ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_A);	
			val[4] += val[0]*val[0];
									
// calculate 2nd order-tensor 
		ElleGetUnodeAttribute(k,&phi1,&Phi,&phi2,EULER_3);
		phi1 *= DTOR;
		Phi *= DTOR;
		phi2 *= DTOR;
		
// direction cosines for [100]
    xyz[0][0]=cos(phi1)*cos(phi2)-sin(phi1)*sin(phi2)*cos(Phi);
    xyz[0][1]=sin(phi1)*cos(phi2)+cos(phi1)*sin(phi2)*cos(Phi);
    xyz[0][2]=sin(phi2)*sin(Phi);
    
// direction cosines for [010]    
    xyz[1][0]=-cos(phi1)*sin(phi2)-sin(phi1)*cos(phi2)*cos(Phi);
    xyz[1][1]=-sin(phi1)*sin(phi2)+cos(phi1)*cos(phi2)*cos(Phi); 
    xyz[1][2]=cos(phi2)*sin(Phi);
    
// direction cosines for [001]
    xyz[2][0]=sin(phi1)*sin(Phi);
    xyz[2][1]=-cos(phi1)*sin(Phi);
    xyz[2][2]=cos(Phi);
    		
		for(i=0; i<3; i++){
			for(j=0; j<3; j++){
				A[i][j] +=xyz[0][i]*xyz[0][j]; 			
			}
		}

		for(i=0; i<3; i++){
			for(j=0; j<3; j++){
				B[i][j] +=xyz[1][i]*xyz[1][j]; 			
			}
		}
		
		for(i=0; i<3; i++){
			for(j=0; j<3; j++){
				C[i][j] +=xyz[2][i]*xyz[2][j]; 			
			}
		}
		
	}		
				
		
		for(i=0; i<3; i++){
			for(j=0; j<3; j++){
				A[i][j] /= max_unodes; 
				B[i][j] /= max_unodes; 
				C[i][j] /= max_unodes; 							
			}
		}
		
		Jacobi_Cyclic_Method( eigenvalues, (double*)eigenvectors, (double*)A, 3);
		printf(" Eigen 2nd [100] %lf %lf %lf ", eigenvalues[0],eigenvalues[1],eigenvalues[2] );	// maximum is HAGB!!
		Jacobi_Cyclic_Method( eigenvalues, (double*)eigenvectors, (double*)B, 3);
		printf(" Eigen 2nd [010] %lf %lf %lf ", eigenvalues[0],eigenvalues[1],eigenvalues[2] );	// maximum is HAGB!!
		Jacobi_Cyclic_Method( eigenvalues, (double*)eigenvectors, (double*)C, 3);
		printf(" Eigen 2nd [001] %lf %lf %lf\n ", eigenvalues[0],eigenvalues[1],eigenvalues[2] );	// maximum is HAGB!! 
*/
 
      err = LoadDataTemp("allstress.out");
	  printf("%i\n", size);
	  
  for (i=0; i<size; i++){ 
	 	      
	D[0][0]=dd[i][0];
	D[1][1]=dd[i][1];
	D[2][2]=dd[i][2];
	D[0][1]=dd[i][5];
	D[1][0]=D[0][1];
	D[1][2]=dd[i][3];
	D[2][1]=D[1][2];
	D[0][2]=dd[i][4];		
	D[2][0]=D[0][2];

		Jacobi_Cyclic_Method( eigenvalues, (double*)eigenvectors, (double*)D, 3);

// re-order max-min eigenvalues,  eigenvalue2 is always (in theory) the intermediate 		
		if ( eigenvalues[0] <= eigenvalues[1] ) {
			tmp=eigenvalues[0];
			eigenvalues[0] = eigenvalues[1];
			eigenvalues[1] = tmp;
			for (j=0; j<3; j++) {
				tmp=eigenvectors[0][j];
			eigenvectors[0][j]= eigenvectors[1][j];
			eigenvectors[1][j]= tmp;	
				}
			}
		
		printf(" Eigenvalues 1, 2 & 3 %i %e %e %e\t ", i, eigenvalues[0],eigenvalues[1],eigenvalues[2] );	// maximum is HAGB!! 
		printf(" Eigenvector1 %lf %lf %lf\t ", eigenvectors[0][0],eigenvectors[0][1],eigenvectors[0][2] );	// maximum is HAGB!! 
		printf(" Eigenvector3 %lf %lf %lf\t ", eigenvectors[1][0],eigenvectors[1][1],eigenvectors[1][2] );	// maximum is HAGB!! 
		printf(" Eigenvector2 %lf %lf %lf\n ", eigenvectors[2][0],eigenvectors[2][1],eigenvectors[2][2] );	// maximum is HAGB!! 		
		
}		
	/*	
// invariants		
		IA=A[0][0]+A[1][1]+A[2][2];
		IIA=A[0][0]*A[1][1]+A[0][0]*A[3][3]+A[2][2]*A[1][1]-A[0][1]*A[0][1]-A[1][2]*A[1][2]-A[0][2]*A[0][2]; 
        matdeterm(A, IIIA); 
        	
// cubic equation 

IA *= -1;
IIIA *= -1;

// roots of the equation


if ()
eigenvalues[0]=   -IA/3
eigenvalues[1]
eigenvalues[2]

*/

/*
// variance ( standard deviation is the sqrt of variance)
mean = val[5]/ElleMaxUnodes();
tmp=0.0; 

for( k=0 ; k<max_unodes; k++) {
			ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_B);	
            tmp += (val[0] - mean) * (val[0] - mean) ;
            }
      
 printf(" <theta> %lf  ",val[1]/ElleMaxUnodes());	// maximum is HAGB!!
 printf("<disloc> %e  ",val[2]/ElleMaxUnodes());	// disloc_density!!		
 printf("<local.> %e  ",(val[3]*val[3]/val[4])/ElleMaxUnodes());	// adimensional strain localisation factor!!
 
 printf("<stress> av. variance %e %e\n  ", mean, tmp/ElleMaxUnodes());

*/

// Histogram
/*
int histArray[32]={0};
double max=0, min=-4;
double interval;

interval= (max-min)/32.0;

for( k=0 ; k<max_unodes; k++) {
	ElleGetUnodeAttribute(k,&val[0],U_ATTRIB_B);
	val[0]=log(val[0]);
	
    for (i=0; i<32; i++){
		 
		if ( val[0]>= min+interval*i && val[0] <= min+interval*(i+1) ) { 
			histArray[i]++;
			break;
		}
	}
}
 
for (i=0; i<32; i++){
printf("%i\t",histArray[i]);
}
printf("\n");
*/
    return(err);
}
void Jacobi_Cyclic_Method(double eigenvalues[3], double *eigenvectors, double *A, int n)
{
   int row, i, j, k, m;
   double *pAk, *pAm, *p_r, *p_e;
   double threshold_norm;
   double threshold;
   double tan_phi, sin_phi, cos_phi, tan2_phi, sin2_phi, cos2_phi;
   double sin_2phi, cos_2phi, cot_2phi;
   double dum1;
   double dum2;
   double dum3;
   double r;
   double max;

                  // Take care of trivial cases
/*
   if ( n < 1) return;
   if ( n == 1) {
      eigenvalues[0] = *A;
      *eigenvectors = 1.0;
      return;
   }
*/
          // Initialize the eigenvalues to the identity matrix.

   for (p_e = eigenvectors, i = 0; i < n; i++)
      for (j = 0; j < n; p_e++, j++)
         if (i == j) *p_e = 1.0; else *p_e = 0.0;
  
            // Calculate the threshold and threshold_norm.
 
   for (threshold = 0.0, pAk = A, i = 0; i < ( n - 1 ); pAk += n, i++) 
      for (j = i + 1; j < n; j++) threshold += *(pAk + j) * *(pAk + j);
   threshold = sqrt(threshold + threshold);
   threshold_norm = threshold * DBL_EPSILON;
   max = threshold + 1.0;
   while (threshold > threshold_norm) {
      threshold /= 10.0;
      if (max < threshold) continue;
      max = 0.0;
      for (pAk = A, k = 0; k < (n-1); pAk += n, k++) {
         for (pAm = pAk + n, m = k + 1; m < n; pAm += n, m++) {
            if ( fabs(*(pAk + m)) < threshold ) continue;

                 // Calculate the sin and cos of the rotation angle which
                 // annihilates A[k][m].

            cot_2phi = 0.5 * ( *(pAk + k) - *(pAm + m) ) / *(pAk + m);
            dum1 = sqrt( cot_2phi * cot_2phi + 1.0);
            if (cot_2phi < 0.0) dum1 = -dum1;
            tan_phi = -cot_2phi + dum1;
            tan2_phi = tan_phi * tan_phi;
            sin2_phi = tan2_phi / (1.0 + tan2_phi);
            cos2_phi = 1.0 - sin2_phi;
            sin_phi = sqrt(sin2_phi);
            if (tan_phi < 0.0) sin_phi = - sin_phi;
            cos_phi = sqrt(cos2_phi); 
            sin_2phi = 2.0 * sin_phi * cos_phi;
            cos_2phi = cos2_phi - sin2_phi;

                     // Rotate columns k and m for both the matrix A 
                     //     and the matrix of eigenvectors.

            p_r = A;
            dum1 = *(pAk + k);
            dum2 = *(pAm + m);
            dum3 = *(pAk + m);
            *(pAk + k) = dum1 * cos2_phi + dum2 * sin2_phi + dum3 * sin_2phi;
            *(pAm + m) = dum1 * sin2_phi + dum2 * cos2_phi - dum3 * sin_2phi;
            *(pAk + m) = 0.0;
            *(pAm + k) = 0.0;
            for (i = 0; i < n; p_r += n, i++) {
               if ( (i == k) || (i == m) ) continue;
               if ( i < k ) dum1 = *(p_r + k); else dum1 = *(pAk + i);
               if ( i < m ) dum2 = *(p_r + m); else dum2 = *(pAm + i);
               dum3 = dum1 * cos_phi + dum2 * sin_phi;
               if ( i < k ) *(p_r + k) = dum3; else *(pAk + i) = dum3;
               dum3 = - dum1 * sin_phi + dum2 * cos_phi;
               if ( i < m ) *(p_r + m) = dum3; else *(pAm + i) = dum3;
            }
            for (p_e = eigenvectors, i = 0; i < n; p_e += n, i++) {
               dum1 = *(p_e + k);
               dum2 = *(p_e + m);
               *(p_e + k) = dum1 * cos_phi + dum2 * sin_phi;
               *(p_e + m) = - dum1 * sin_phi + dum2 * cos_phi;
            }
         }
         for (i = 0; i < n; i++)
            if ( i == k ) continue;
            else if ( max < fabs(*(pAk + i))) max = fabs(*(pAk + i));
      }
   }
   for (pAk = A, k = 0; k < n; pAk += n, k++) 
   {	eigenvalues[k] = *(pAk + k); 
	}

}
////////////////////////////////////////////////////////////////////////////////
// File: jacobi_cyclic_method.c                                               //
// Routines:                                                                  //
//    Jacobi_Cyclic_Method                                                    //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//  void Jacobi_Cyclic_Method                                                 //
//            (double eigenvalues[], double *eigenvectors, double *A, int n)  //
//                                                                            //
//  Description:                                                              //
//     Find the eigenvalues and eigenvectors of a symmetric n x n matrix A    //
//     using the Jacobi method. Upon return, the input matrix A will have     //
//     been modified.                                                         //
//     The Jacobi procedure for finding the eigenvalues and eigenvectors of a //
//     symmetric matrix A is based on finding a similarity transformation     //
//     which diagonalizes A.  The similarity transformation is given by a     //
//     product of a sequence of orthogonal (rotation) matrices each of which  //
//     annihilates an off-diagonal element and its transpose.  The rotation   //
//     effects only the rows and columns containing the off-diagonal element  //
//     and its transpose, i.e. if a[i][j] is an off-diagonal element, then    //
//     the orthogonal transformation rotates rows a[i][] and a[j][], and      //
//     equivalently it rotates columns a[][i] and a[][j], so that a[i][j] = 0 //
//     and a[j][i] = 0.                                                       //
//     The cyclic Jacobi method considers the off-diagonal elements in the    //
//     following order: (0,1),(0,2),...,(0,n-1),(1,2),...,(n-2,n-1).  If the  //
//     the magnitude of the off-diagonal element is greater than a treshold,  //
//     then a rotation is performed to annihilate that off-diagnonal element. //
//     The process described above is called a sweep.  After a sweep has been //
//     completed, the threshold is lowered and another sweep is performed     //
//     with the new threshold. This process is completed until the final      //
//     sweep is performed with the final threshold.                           //
//     The orthogonal transformation which annihilates the matrix element     //
//     a[k][m], k != m, is Q = q[i][j], where q[i][j] = 0 if i != j, i,j != k //
//     i,j != m and q[i][j] = 1 if i = j, i,j != k, i,j != m, q[k][k] =       //
//     q[m][m] = cos(phi), q[k][m] = -sin(phi), and q[m][k] = sin(phi), where //
//     the angle phi is determined by requiring a[k][m] -> 0.  This condition //
//     on the angle phi is equivalent to                                      //
//               cot(2 phi) = 0.5 * (a[k][k] - a[m][m]) / a[k][m]             //
//     Since tan(2 phi) = 2 tan(phi) / (1.0 - tan(phi)^2),                    //
//               tan(phi)^2 + 2cot(2 phi) * tan(phi) - 1 = 0.                 //
//     Solving for tan(phi), choosing the solution with smallest magnitude,   //
//       tan(phi) = - cot(2 phi) + sgn(cot(2 phi)) sqrt(cot(2phi)^2 + 1).     //
//     Then cos(phi)^2 = 1 / (1 + tan(phi)^2) and sin(phi)^2 = 1 - cos(phi)^2 //
//     Finally by taking the sqrts and assigning the sign to the sin the same //
//     as that of the tan, the orthogonal transformation Q is determined.     //
//     Let A" be the matrix obtained from the matrix A by applying the        //
//     similarity transformation Q, since Q is orthogonal, A" = Q'AQ, where Q'//
//     is the transpose of Q (which is the same as the inverse of Q).  Then   //
//         a"[i][j] = Q'[i][p] a[p][q] Q[q][j] = Q[p][i] a[p][q] Q[q][j],     //
//     where repeated indices are summed over.                                //
//     If i is not equal to either k or m, then Q[i][j] is the Kronecker      //
//     delta.   So if both i and j are not equal to either k or m,            //
//                                a"[i][j] = a[i][j].                         //
//     If i = k, j = k,                                                       //
//        a"[k][k] =                                                          //
//           a[k][k]*cos(phi)^2 + a[k][m]*sin(2 phi) + a[m][m]*sin(phi)^2     //
//     If i = k, j = m,                                                       //
//        a"[k][m] = a"[m][k] = 0 =                                           //
//           a[k][m]*cos(2 phi) + 0.5 * (a[m][m] - a[k][k])*sin(2 phi)        //
//     If i = k, j != k or m,                                                 //
//        a"[k][j] = a"[j][k] = a[k][j] * cos(phi) + a[m][j] * sin(phi)       //
//     If i = m, j = k, a"[m][k] = 0                                          //
//     If i = m, j = m,                                                       //
//        a"[m][m] =                                                          //
//           a[m][m]*cos(phi)^2 - a[k][m]*sin(2 phi) + a[k][k]*sin(phi)^2     //
//     If i= m, j != k or m,                                                  //
//        a"[m][j] = a"[j][m] = a[m][j] * cos(phi) - a[k][j] * sin(phi)       //
//                                                                            //
//     If X is the matrix of normalized eigenvectors stored so that the ith   //
//     column corresponds to the ith eigenvalue, then AX = X Lamda, where     //
//     Lambda is the diagonal matrix with the ith eigenvalue stored at        //
//     Lambda[i][i], i.e. X'AX = Lambda and X is orthogonal, the eigenvectors //
//     are normalized and orthogonal.  So, X = Q1 Q2 ... Qs, where Qi is      //
//     the ith orthogonal matrix,  i.e. X can be recursively approximated by  //
//     the recursion relation X" = X Q, where Q is the orthogonal matrix and  //
//     the initial estimate for X is the identity matrix.                     //
//     If j = k, then x"[i][k] = x[i][k] * cos(phi) + x[i][m] * sin(phi),     //
//     if j = m, then x"[i][m] = x[i][m] * cos(phi) - x[i][k] * sin(phi), and //
//     if j != k and j != m, then x"[i][j] = x[i][j].                         //
//                                                                            //
//  Arguments:                                                                //
//     double  eigenvalues                                                    //
//        Array of dimension n, which upon return contains the eigenvalues of //
//        the matrix A.                                                       //
//     double* eigenvectors                                                   //
//        Matrix of eigenvectors, the ith column of which contains an         //
//        eigenvector corresponding to the ith eigenvalue in the array        //
//        eigenvalues.                                                        //
//     double* A                                                              //
//        Pointer to the first element of the symmetric n x n matrix A. The   //
//        input matrix A is modified during the process.                      //
//     int     n                                                              //
//        The dimension of the array eigenvalues, number of columns and rows  //
//        of the matrices eigenvectors and A.                                 //
//                                                                            //
//  Return Values:                                                            //
//     Function is of type void.                                              //
//                                                                            //
//  Example:                                                                  //
//     #define N                                                              //
//     double A[N][N], double eigenvalues[N], double eigenvectors[N][N]       //
//                                                                            //
//     (your code to initialize the matrix A )                                //
//                                                                            //
//     Jacobi_Cyclic_Method(eigenvalues, (double*)eigenvectors,               //
//                                                          (double *) A, N); //
////////////////////////////////////////////////////////////////////////////////
//      

/*
void matdeterm(double a[3][3], double *da)
{                                         
    //double da;
    
                                
    *da=-a[0][2]*a[1][1]*a[2][0]-a[0][0]*a[1][2]*a[2][1]-a[0][1]*a[1][0]*a[2][2]+
        a[0][0]*a[1][1]*a[2][2]+a[0][1]*a[1][2]*a[2][0]+a[0][2]*a[1][0]*a[2][1];
  
}
* /
/*
void matadj(double a[3][3], double ia[3][3])
{                                         
    double d[3][3],b[3][3],z;
    int i,j,k;

    b[0][0]=(a[1][1]*a[2][2]-a[1][2]*a[2][1]);
    b[0][1]=-((a[1][0]*a[2][2]-a[1][2]*a[2][0]));
    b[0][2]=(a[1][0]*a[2][1]-a[1][1]*a[2][0]);
    b[1][0]=-((a[0][1]*a[2][2]-a[0][2]*a[2][1]));
    b[1][1]=(a[0][0]*a[2][2]-a[0][2]*a[2][0]);
    b[1][2]=-((a[0][0]*a[2][1]-a[0][1]*a[2][0]));
    b[2][0]=(a[0][1]*a[1][2]-a[0][2]*a[1][1]);
    b[2][1]=-((a[0][0]*a[1][2]-a[0][2]*a[1][0]));
    b[2][2]=(a[0][0]*a[1][1]-a[0][1]*a[1][0]);
                                
    

    //transpose

    for(i=0;i<3;i++)
    {
	for(j=0;j<3;j++)
	{
	    ia[i][j]=b[j][i];
	}
    }

}	
*/	
int LoadDataTemp(const char *fname)
{
        /*
         * Read the new cellbox size and displacements
         */
    int err=0;
    int id,i=0,j=0;
    
    double val[6];

    ifstream datafile(fname);
    if (!datafile) return(OPEN_ERR);
    while (datafile) {
        datafile >> val[0] >> val[1] >> val[2] >> val[3] >> val[4] >> val[5];
        // printf(" %i %lf %lf %lf %lf %lf %lf\n", i, val[0],val[1],val[2],val[3],val[4],val[5]);
        for (j=0;j<=6;j++) dd[i][j]=(double)val[j]; // using the same structure than in VPFFT (Voigt notation)
	    i=i+1;
    }
    datafile.close();
    size=i;
    return(err);
}
