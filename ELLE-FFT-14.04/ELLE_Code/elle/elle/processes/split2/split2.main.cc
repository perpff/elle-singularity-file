
/*
 *  main.cc
 */

#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "parseopts.h"
#include "init.h"
#include "runopts.h"
#include "stats.h"
#include "setup.h"



int main(int argc, char **argv)
{
    int err=0;
    extern int Init_Split2(void);

    ElleInit();

    ElleSetInitFunction(Init_Split2);

    if (err=ParseOptions(argc,argv))
        OnError("",err);
    //ES_SetstatsInterval(50);

    ElleSetSaveFileRoot("split2");

    if (ElleDisplay()) SetupApp(argc,argv);

    StartApp();

    return(0);
}
