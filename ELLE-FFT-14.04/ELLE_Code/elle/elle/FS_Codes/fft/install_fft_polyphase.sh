#!/bin/bash
# Install and compile the 2 phase FFT versions with 128x128 and 256x256 unodes. Executables will be copied to /home/username/programs/elle/elle/binwx or any path specified in "$ELLEPATH"
# Version 150314_1558
DEBUG=
#DEBUG="echo"

prefix="FS_"

$DEBUG cd version128/

$DEBUG gfortran -O3 $prefix"PPC15E-9.05.FOR" -o FFT_vs128
$DEBUG cp FFT_vs128 $ELLEPATH

$DEBUG cd ../version256/

$DEBUG gfortran -O3 $prefix"PPC15E-9.05.FOR" -o FFT_vs256
$DEBUG cp FFT_vs256 $ELLEPATH

$DEBUG cd ../version512/

$DEBUG gfortran -O3 $prefix"PPC15E-9.05.FOR" -o FFT_vs512
$DEBUG cp FFT_vs512 $ELLEPATH

$DEBUG cd ..
echo " "
echo "Finished with installing polyphase 128-, 256- and 512-FFT versions..."
