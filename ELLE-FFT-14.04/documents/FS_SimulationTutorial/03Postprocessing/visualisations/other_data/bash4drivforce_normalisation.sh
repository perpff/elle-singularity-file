#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

$DEBUG rm *~

FILEROOT=filename # Name of the input Elle files without the "_stepXXX.elle" at the end
OUTROOT=$FILEROOT"_bnode_info"

STARTSTEP=1 # If you want to start with file "_step001", type 1
STEPS=70 # Maximum number of steps
INCREMENT=1

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)) ) 
    echo "REMEMBER TO SWITCH ON THE CORRECT FUNCTION IN STATISTICS CODE!"
    echo "Normalising Estrain to Esurf_mean: step "$i

    $DEBUG cp $FILEROOT"_step"$step.elle tmp.elle
    $DEBUG tidy -i tmp.elle -u 0 0 0 0 -1 -n
    $DEBUG mv tidy001.elle tmp.elle
    $DEBUG FS_statistics -i tmp.elle -u 4 -1 -n # call function by using -u 4 -1 (or s.th. negative for 2nd input)
    $DEBUG mv norm_Estrain.elle tmp.elle

    $DEBUG FS_create_plotlayer -i tmp.elle -u -1 2 -n
    $DEBUG mv with_plotlayer.elle tmp.elle

    $DEBUG FS_scalefile -i tmp.elle -u 1.5 1.5 -n
    $DEBUG mv scaled_file.elle tmp.elle

    $DEBUG mv tmp.elle $OUTROOT"_step"$step.elle
    
done
