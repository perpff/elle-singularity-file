#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#
DEBUG=
#DEBUG="echo"
$DEBUG rm *~
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   INPUT                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

FILEROOT=filename

STARTSTEP=1
STEPS=70
INCREMENT=1

OUTPATH="grid_images/"     # Path where all eunodes.data files are stored+the ugrid files
PLOT_UGRID_ORIENTATION=1 # defines orientation of lines of grid, cf. readme to FS_plot_ugrid 0: horizontal, 1: vertical
PLOT_UGRID_SPACING=2     # spacing of the lines of grid, cf. readme to FS_plot_ugrid
SCALE=4 # needs proper adjustment to fit every box during pure shearing on ps file, use 8 for simple and 4 for pure shear
THRESH=0.15 # any pair of unodes with separation < this value will not be connected to a grid line, 0.15 is a good value for that
EXCLUDEPHASE=0 # Do not plot lines for this phase, type 0 to not use this option (which is actually recommended)

SAVE_PNGS=1             # set to 0/1 to switch additional creation of pngs off/on, check code below for creation settings
RESOLUTION=200           # set resolution (dpi) for png images

CROP_IMAGE=0            # set to 0/1 if you wish or wish not to crop the PNG image by the parameters set below
WIDTH=1652
HEIGHT=1642
XOFFSET=0
YOFFSET=558

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                       GET ALL EUNODE.DATA FILES                           #
#               ADD "UNODES" AS FIRST LINE AND PLOT UGRID                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

if [ ! -d "$OUTPATH" ]; then
    # If the directory $OUTPATH does not exist: create it:
    $DEBUG mkdir $OUTPATH    
fi

echo "Plotting to postscript images ..."

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) )) 
do
    step=$(printf "%03d" $(($i)) )
    echo "... image "$step
    # PERFORM THE UGRID-PLOTTING AND CCCCREATE PNG
    FILENAME=$FILEROOT"_step"$step
    $DEBUG FS_plot_ugrid -i $FILENAME.elle -u $PLOT_UGRID_ORIENTATION $PLOT_UGRID_SPACING $SCALE $THRESH $EXCLUDEPHASE -n  
      
    if [ 1 -eq "$SAVE_PNGS" ]; then
    $DEBUG convert -density $RESOLUTION -background white -flatten $FILENAME"_ugrid.ps" $FILENAME"_ugrid.png"
        # -density 300: convert to an image with e.g. 72 dpi or whatever resolution
        # -background white -flatten: put layer is on a white background, flatten image afterwards
        # Crop image by user defined parameter:
        # command: -crop widthxheight+xoffset+yoffset --> (x,y)=(0,0) is in top left corner
        if [ 1 -eq "$CROP_IMAGE" ]; then
            $DEBUG convert $FILENAME"_ugrid.png" -crop $WIDTH"x"$HEIGHT"+"$XOFFSET"+"$YOFFSET $FILENAME"_ugrid.png" 
        fi
    fi

    # MOVE PNG AND PS FILE TO OUTPATH
    if [ 1 -eq "$SAVE_PNGS" ]; then
        $DEBUG mv $FILENAME"_ugrid.ps" $OUTPATH
    fi
    $DEBUG mv $FILENAME"_ugrid.png" $OUTPATH

done

$DEBUG rm *elle *~
$DEBUG mv grid_images/*png .
$DEBUG mv grid_images/*.ps .
$DEBUG rm -r grid_images

echo "~~~~~ Finished ~~~~~"

