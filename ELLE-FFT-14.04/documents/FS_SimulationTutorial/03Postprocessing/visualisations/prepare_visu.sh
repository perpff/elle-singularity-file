#!/bin/bash
# Prepare visualisation, load data etc.
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#
rm *~
DEBUG=
#DEBUG=echo

SIMULATIONDIR="../02RunningTheSimulation/" # with "/" at the end
FILEROOT=mysimulation # without "_stepXXX.elle"
STARTSTEP=1 
STEPS=70
INCREMENT=1

USE_PLOTFILES=1 # 1 for yes, 0 for no

# Change FILEROOT in bash scripts to extract data from elle files:
$DEBUG sed -i '18s/.*/FILEROOT='$FILEROOT'/' other_data/bash4grainsizestats.sh
$DEBUG sed -i '21s/.*/STARTSTEP='$STARTSTEP'/' other_data/bash4grainsizestats.sh
$DEBUG sed -i '22s/.*/STEPS='$STEPS'/' other_data/bash4grainsizestats.sh
$DEBUG sed -i '23s/.*/INCREMENT='$INCREMENT'/' other_data/bash4grainsizestats.sh

$DEBUG sed -i '22s/.*/FILEROOT='$FILEROOT'/' other_data/bash4slipsystemact.sh
$DEBUG sed -i '25s/.*/STARTSTEP='$STARTSTEP'/' other_data/bash4slipsystemact.sh
$DEBUG sed -i '26s/.*/STEPS='$STEPS'/' other_data/bash4slipsystemact.sh
$DEBUG sed -i '27s/.*/INCREMENT='$INCREMENT'/' other_data/bash4slipsystemact.sh

$DEBUG sed -i '11s/.*/FILEROOT='$FILEROOT'/' other_data/bash4drivforce_normalisation.sh
$DEBUG sed -i '14s/.*/STARTSTEP='$STARTSTEP'/' other_data/bash4drivforce_normalisation.sh
$DEBUG sed -i '15s/.*/STEPS='$STEPS'/' other_data/bash4drivforce_normalisation.sh
$DEBUG sed -i '16s/.*/INCREMENT='$INCREMENT'/' other_data/bash4drivforce_normalisation.sh

$DEBUG sed -i '10s/.*/FILEROOT='$FILEROOT'/' other_data/bash4perimeterratios.sh
$DEBUG sed -i '12s/.*/STARTSTEP='$STARTSTEP'/' other_data/bash4perimeterratios.sh
$DEBUG sed -i '13s/.*/STEPS='$STEPS'/' other_data/bash4perimeterratios.sh
$DEBUG sed -i '14s/.*/INCREMENT='$INCREMENT'/' other_data/bash4perimeterratios.sh

$DEBUG sed -i '11s/.*/FILEROOT='$FILEROOT'/' other_data/bash4localizationfactor.sh
$DEBUG sed -i '13s/.*/STARTSTEP='$STARTSTEP'/' other_data/bash4localizationfactor.sh
$DEBUG sed -i '14s/.*/STEPS='$STEPS'/' other_data/bash4localizationfactor.sh
$DEBUG sed -i '15s/.*/INCREMENT='$INCREMENT'/' other_data/bash4localizationfactor.sh


if [ $USE_PLOTFILES -eq 1 ];then
    $DEBUG sed -i '17s/.*/FILEROOT='$FILEROOT'_plotfile/' stereos/bash4stereos.sh
else
    $DEBUG sed -i '17s/.*/FILEROOT='$FILEROOT'/' stereos/bash4stereos.sh
fi
$DEBUG sed -i '23s/.*/STARTSTEP='$STARTSTEP'/' stereos/bash4stereos.sh
$DEBUG sed -i '24s/.*/STEPS='$STEPS'/' stereos/bash4stereos.sh
$DEBUG sed -i '25s/.*/INCREMENT='$INCREMENT'/' stereos/bash4stereos.sh

$DEBUG sed -i '12s/.*/FILEROOT='$FILEROOT'/' ugrids/bash4ugrid.sh
$DEBUG sed -i '14s/.*/STARTSTEP='$STARTSTEP'/' ugrids/bash4ugrid.sh
$DEBUG sed -i '15s/.*/STEPS='$STEPS'/' ugrids/bash4ugrid.sh
$DEBUG sed -i '16s/.*/INCREMENT='$INCREMENT'/' ugrids/bash4ugrid.sh

PWDIR=$(pwd)
cd $SIMULATIONDIR

cd results/
echo "Copying results to 'other_data'"
$DEBUG cp AllOutData.txt $PWDIR"/other_data/"$FILEROOT"_AllOutData.txt"
$DEBUG cp *elle $PWDIR"/other_data/"
echo "Copying results to 'phases'"
$DEBUG cp *elle $PWDIR"/phases/"
echo "Copying results to 'ugrids'"
$DEBUG cp *elle $PWDIR"/ugrids/"

if [ $USE_PLOTFILES -eq 1 ];then       
    cd plotfiles/
fi
echo "Copying results to 'dislocden'"
$DEBUG cp *elle $PWDIR"/dislocden/"

echo "Copying results to 'eqedot'"
$DEBUG cp *elle $PWDIR"/eqedot/"

echo "Copying results to 'LPO'"
$DEBUG cp *elle $PWDIR"/LPO/"

echo "Copying results to 'stereos'"
$DEBUG cp *elle $PWDIR"/stereos/"

echo "Copying results to 'basalact'"
$DEBUG cp *elle $PWDIR"/basalact/"

cd $PWDIR"/other_data/"

$DEBUG chmod +x bash4grainsizestats.sh
$DEBUG bash4grainsizestats.sh
$DEBUG chmod +x bash4slipsystemact.sh
$DEBUG bash4slipsystemact.sh
$DEBUG chmod +x bash4localizationfactor.sh
$DEBUG bash4localizationfactor.sh
$DEBUG chmod +x bash4perimeterratios.sh
$DEBUG bash4perimeterratios.sh
$DEBUG rm *elle *~

cd ../stereos/
$DEBUG chmod +x bash4stereos.sh
$DEBUG bash4stereos.sh
$DEBUG rm *elle *~
$DEBUG mv stereos/*png .
$DEBUG rm -r stereos

cd ../ugrids
$DEBUG chmod +x bash4ugrid.sh
$DEBUG bash4ugrid.sh
$DEBUG rm *elle *~
$DEBUG mv grid_images/*png .
$DEBUG mv grid_images/*.ps .
$DEBUG rm -r grid_images

echo "~~~~~ FINISHED ~~~~~"
echo " "
echo "Please open showelle with an Elle file in every plot-folder"
echo "and choose Graphics -> Save -> Save whole run"
echo " "
