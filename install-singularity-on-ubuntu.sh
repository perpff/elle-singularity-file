#!/bin/bash

# we need to install go
sudo apt update
sudo apt install -y golang

# we also want checkinstall
sudo apt install -y checkinstall

# in case an earlyversion is installed 
# (where the packagename was wrong!)
# make sure to uninstall this.
sudo dpkg -r builddir 

# also remove previous versions
sudo dpkg -r singularity-container

ver=3.8.4

echo "Singularity-version to be installed:" $ver

# download singularity
wget https://github.com/singularityware/singularity/releases/download/v$ver/singularity-$ver.tar.gz

# decompress
tar -xzvf singularity-$ver.tar.gz

# build
cd singularity-$ver/
./mconfig
cd builddir/
make
sudo checkinstall --pkgname singularity-container --pkgversion $ver -y

sudo chown $USER singularity-container*
mv singularity-container* ../../.

cd ../..
sudo rm -rf singularity-$ver
rm -f singularity-$ver.tar.gz
