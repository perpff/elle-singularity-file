# Elle singularity-images

You can run and develop on Elle using the singularity images you will find in this repository.

In this Readme you will find:

1) Instructions how to install singularity on your system
2) A description of this repo and what it contains

## 1) Instructions to install singularity on your system

On Linux:

- on Arch-based Linux-systems (like Manjaro, Endeavour or plain Arch) Singularity is in the repos. All you have to do in order to install it is to type in a terminal:
  `sudo pacman -S singularity-container`
- on Ubuntu / Debian it's not quite as easy, because 'singularity' is not in the official repositories. You have the following options:
  - **Preferred method:** install using the script contained in this gitlab-repo: 
    - download the shell script 'install-singularity-3.8.3-on-ubuntu.sh' from this repo
    - open a terminal, and cd to the download-directory
    - execute: `bash install-singularity-3.8.3-on-ubuntu.sh`
    - This will install dependencies, download sources, compile, create a .deb-file and install it.
  - alternatively you can try the _neuroscience_-repository
    - **heads-up 1: this is usually a very old version!**
    - **heads-up 2: often doesn't work at all..**
    - http://neuro.debian.net/install_pkg.html?p=singularity-container

On Windows / Mac:

- Apparently there is a guide how to install singularity on Windows / Mac here: https://singularity.hpcng.org/admin-docs/3.8/installation.html#installation-on-windows-or-mac
- just a heads-up: at its core 'Singularity' is a _Linux_ application. I do not know what loops you have to jump through to install it on Windows / Mac.

## 2) About the contents of this repository

This repo contains 

- singularity-images with Elle preinstalled, based on Ubuntu 14.04 and Ubuntu 16.04. 
- the build files to create these images
- the ELLE-FFT code, which is sadly not compatible with the current Elle-version on Sourceforge. 

There are 2 versions of images:

- the popular ELLE-FFT is installed in the elle-14.04-simg image. It is identical to the version created during Florian Steinbachs thesis. This version of Elle is not compatible with the version in the source-forge repo any more!!
- the standard Elle from sourceforge is installed in the elle-16.04.simg image. This version of Elle does currently not run on any Ubuntu version other than 16.04.

Information about running Elle is included as .md/.pdf files and at some point in the future maybe in this Readme.

The singularity scripts do currently the following:

1) Download Ubuntu 14.04 / 16.04 from dockerhub
2) Install the dependencies and the build-environement to compile wxGTK and subsequently Elle
3) Download wxGTK 2.8 and compile and install it
4) Elle code:
   1) _either_ add the Elle code (_included in this repo_) to the 14,04 image, 
   2) _or_ download the current sources from sourceforge for the 16.04 image 
5) Compile Elle and install it either in /programs/elle or in /programs/elle/ele
6) Set the PATH variable to point to the Elle executables.

You can create an image of your own by running `singularity build elle-14.04.simg Singularity-14.04-elle.def` (or similar for the 16.04 version). OR simply download the image files from the repo. If building your own version of the 14.04-image: don't forget to download the corresponding Elle-code (ELLE-FFT) and to put it into the same directory, where you are keeping the Singularity*.def file.

The older, now obsolete, 'bionic' (Ubuntu 18.04) version created a functional image - but showelle kept crashing!.. :(

Most basic usage:
- Run a shell inside the container by invoking `singularity shell elle-1[4,6].4.simg` in the directory where the *.simg file lives.
- Just type `showelle` to check if it works, and open a .elle file on your system.
- From there, you _should_ be able to use Elle almost as usual, as long as you don't intend to recompile.

If you need to compile your own Elle-version:
- You can also use the shell opened with the command above in order to compile and run your own source code somewhere on your machine.
- Better: have a look at the included documentation. It's probably short enough.
