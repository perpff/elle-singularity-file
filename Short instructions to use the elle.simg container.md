# Short instructions to use the elle.simg container

## Intro

Containers package the user space of an operating system inside a single container file or a directory. The 'elle.simg'-container contains the user space of Ubuntu's Bionic release + packages that you need to run 'elle'. This way, you can run elle <u>on any distribution</u>, provided that singularity is installed! You can convert the downloaded (*read-only*) singularity image to a *writable* image or to a *sandbox*, which you can also use to edit and compile the included elle-source code.

There are altogether 3 different ways to use the elle-singularity container. You can use it to:

1. Simply use it "as-is" in order to run elle. If you use it that way you can not edit the elle-source code,
2. you can convert it to a *writable* container or a *sandbox*, which allows you to edit and compile the included source code *in* the container, or
3. you can use it as a build-environment and compile source code in your home-directory using the compiler and dependencies inside the container.

## Install singularity on machine

Please compare the Readme in the gitlab repo. Currently, installation on Ubunut requires a script that you can download from the repo. Other distributions might have the pacakge in their repositories.

## Convert the downloaded container to a writable format

What you downloaded is a squashfs container (squahsf = a compressed file system) called elle.simg. If you want to convert it to a writable ext3 container called elle-develop.img you could do this: 

```sh
sudo singularity build --writable elle-develop.img elle.simg
```

 Similarly, to convert it to a writable <u>directory</u> (a sandbox): 

```sh
singularity build --sandbox elle-develop/ elle.simg
```

Depending on how you want to use the container, you may not have to convert the file.

## Use the container to work with elle

### Using a read-only container

I recommend to open a shell inside the elle.simg container. That way you have still full read-write access to your home-directory, but you can start the elle-binaries with the necessary parameters from the container. Simply run:

`singularity shell elle.simg`.

You will find Elle now under /programs/elle. Also, the elle-path is in .bashrc, so you can start eg showelle from where-ever you are with a simple:

`showelle`

on the command line. 

This is the easiest, but also the least flexible way to use Elle. 

### Using the container file as a build environment and a runtime for your local code

An easy option, if you have trouble to compile the elle-code on your machine, is to download the development code from sourceforge somewhere into your home directory:

`git clone https://git.code.sf.net/p/elle/git elle-git`

Put the singularity container into the elle directory (or somewhere else where you can easily find it), and then in a terminal:

`singularity shell elle.simg`

Your terminal is now running inside the container. To install Elle, you can now do the usual:

`sh install.sh wx`

and Elle will compile and install in your chosen directory.

To run elle or to start processes in the usual way you can simply start a shell inside the singularity container again. Just remember that you have to use your local elle-install, not the one bundled in the container. If you want to start a local latte process, for instance, you need to tell your terminal its exact location, otherwise it will start the one under /programs/elle inside the container:

```shell
cd /home/you/path/to/elle
singularity shell path/to/elle.simg
./binwx/elle_latte -i your_options

#or:
processes/latte/elle_latte -i ...
```



### Using a *writable* elle singularity container

After you converted the downloaded container file to a writable format (see above: either to an ext3 container or a sandbox), you can edit and compile the files under /programs/elle. In order to do so you need to open a shell inside the image with the following parameters:

```sh
sudo singularity shell --writable elle.simg
```

Note the leading 'sudo' and the option '--writable'.
