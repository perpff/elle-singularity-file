#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

rm *~

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   INPUT                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

OUTDIR=stereos

FILEROOT=filename # Name until "_step" and without the 3 digit step number
OUTROOT=$FILEROOT"_ice_stereo" # Output .ps- filename until "_step" and without the 3 digit step number

PLOT_FREQUENCY=10 # Every "PLOT_FREQUENCEY"-th unode will be represented in the stereogram
CAXIS_OUT=0 # Type 1 if c-axis data should be exported to a textfile, 0 if not

STARTSTEP=1 # First step to perform, if you want to plot all data starting with step001, type 1
STEPS=75 # Total number of steps
INCREMENT=1

SAVE_PNGS=1             # set to 0/1 to switch additional creation of pngs off/on, check code below for creation settings
RESOLUTION=200           # set resolution (dpi) for png images

CROP_IMAGE=1            # set to 0/1 if you wish or wish not to crop the PNG image by the parameters set below
WIDTH=936
HEIGHT=783
XOFFSET=0
YOFFSET=0

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   BEGIN PLOTTING                          #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


if [ ! -d "$OUTDIR" ]; then
    # If the directory $OUTDIR does not exist: create it:
    $DEBUG mkdir $OUTDIR    
fi

echo "Plotting stereonets..."

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)))     
    echo "... step "$step" of "$STEPS
    filename=$FILEROOT"_step"$step.elle
    outfilenamePS=$OUTROOT"_step"$step.ps
    $DEBUG plotaxes -i $filename -u 0 $PLOT_FREQUENCY $CAXIS_OUT -n
    $DEBUG mv $filename"_ax.ps" $outfilenamePS

    if [ 1 -eq "$SAVE_PNGS" ]; then
    
    outfilenamePNG=$OUTROOT"_step"$step.png
    $DEBUG convert -density $RESOLUTION -background white -flatten $outfilenamePS $outfilenamePNG
        # -density 300: convert to an image with e.g. 72 dpi or whatever resolution
        # -background white -flatten: put layer is on a white background, flatten image afterwards
        # Crop image by user defined parameter:
        # command: -crop widthxheight+xoffset+yoffset --> (x,y)=(0,0) is in top left corner
        if [ 1 -eq "$CROP_IMAGE" ]; then
            $DEBUG convert $outfilenamePNG -crop $WIDTH"x"$HEIGHT"+"$XOFFSET"+"$YOFFSET $outfilenamePNG
        fi
        $DEBUG mv $outfilenamePNG $OUTDIR
    fi

    $DEBUG mv $outfilenamePS $OUTDIR
    
done


$DEBUG rm *elle *~
$DEBUG mv stereos/*png .
$DEBUG rm -r stereos

echo "~~~~~ Finished ~~~~~"

