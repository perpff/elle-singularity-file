#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

$DEBUG rm *~

FILEROOT=filename

STARTSTEP=1 # If you want to start with file "_step001", type 1
STEPS=70 # Maximum number of steps
INCREMENT=1

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)) ) 
    echo "Step "$step

    $DEBUG FS_statistics -i $FILEROOT"_step"$step.elle -u 4 -n
    
done

$DEBUG mv LocalizationFactors.txt $FILEROOT"_LocalizationFactors.txt"
