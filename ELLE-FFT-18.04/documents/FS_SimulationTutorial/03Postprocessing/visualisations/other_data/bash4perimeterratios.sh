#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

$DEBUG rm *~
FILEROOT=filename # Name until "_step" and without the 3 digit step number

STARTSTEP=1 # If you want to start with file "_step001", type 1
STEPS=70 # Maximum number of steps
INCREMENT=1

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)) ) 
    echo "~~~ STEP "$i" OF "$STEPS" ~~~"
    $DEBUG FS_statistics -i $FILEROOT"_step"$step.elle -u 4 1 -n
    $DEBUG mv "PerimeterRatios.txt" $FILEROOT"_perimeterratios_step"$step".txt"    
done

$DEBUG zip $FILEROOT"_AllPerimeterRatios.zip" *perimeterratios_step*
$DEBUG rm *perimeterratios_step*
