#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

rm *~

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   INPUT                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Loads all all.out files from folders called step001 - stepxxx (xxx is number of steps)
# and extracts the data from it using FS_statistics -i dummy.elle -u 1 1 -n 
# A DUMMY ELLE FILE IS CREATED AND AFTERWARDS DELETED FOR THAT, ACTUALLY ONLY THE all.out FILE NEEDS
# TO BE IN THE SAME DIRECTORY FROM WHERE FS_statistics IS CALLED

STARTSTEP=1 # If you want to start with folder "step001" type 1
STEPS=70 # Maximum number of steps
INCREMENT=1
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   BEGIN CALCULATIONS                      #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Create dummy Elle file to be able to call FS_statistics:
$DEBUG echo 'UNODES' >dummy.elle

echo "Reading all.out data..."

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)) ) 
    echo "Step "$step
    $DEBUG cp "step"$step"/all"$step".out" all.out
    $DEBUG FS_statistics -i dummy.elle -u 1 1 -n
    
done

# Remove dummy Elle file
$DEBUG rm dummy.elle all.out
