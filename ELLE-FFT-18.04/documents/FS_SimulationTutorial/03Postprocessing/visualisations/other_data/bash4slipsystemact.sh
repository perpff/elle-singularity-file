#!/bin/bash
#
# By Florian Steinbach, florian.steinbach@uni-tuebingen.de
#

DEBUG=
#DEBUG=echo

rm *~

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   INPUT                                   #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Using FS_statistics, which is assuming that basal activity is stored in U_ATTRIB_D and prism. activity
# is stored in U_ATTRIB_E. Activities are read from all unodes not belonging to phase 2 (assumed to be air)
# and mean activities are calculated from actual number of unodes where phase is not 2
#
# Since tex.out does only provide basal and prismatic activities, pyramidal activity is calculated, assuming that the
# sum of all activities is 1 (100%)

FILEROOT=filename # Name until "_step" and without the 3 digit step number
OUTROOT=$FILEROOT"_MeanSlipSysAct" # Output .txt filename

STARTSTEP=1 # If you want to start with file "_step001", type 1
STEPS=70 # Maximum number of steps
INCREMENT=1

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                   BEGIN CALCULATIONS                      #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


echo "Reading mean slip system activities..."

for (( i=$STARTSTEP; i<=$STEPS; i=$(($i+$INCREMENT)) ))
do
    step=$(printf "%03d" $(($i)))     
    echo "Reading slip system activities from step "$step" of "$STEPS
    
    $DEBUG FS_statistics -i $FILEROOT"_step"$step.elle -u 3 1 -n

done

$DEBUG mv SlipSystemAct.txt $OUTROOT.txt
