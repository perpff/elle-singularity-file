# include <iostream>
# include <stdio.h>
# include "fluid_lattice.h"

using namespace std;

//-------------------------------------------------------------------------------
// fluid lattice February 2016, Daniel, added Advection/Diffusion 2018 Daniel
//
// based on pressure lattice by Irfan Ghani
// ADI from Till Sachau's temperature diffusion code
// 
// Derivation of Pressure diffusion equation based on Renauld Toussaint's work
//
// This code deals with a compressible fluid, has a square lattice that is linked
// to the particle lattice of latte. Fluid lattice cell width is twice that of 
// the repulsion box of the particle lattice. Repulsion box is used to link both
// 
// Fluid lattice deals with variations of fluid pressure per node assuming Darcy
// flow of fluids in the background. Reads in porosity from the solid code, 
// converts that into a permeability. Gives back fluid pressure gradients as 
// Fluid forces in X and Y that act on the solid. 
//
//-------------------------------------------------------------------------------

// ---------------------------------------------------------------
// Constructor of Fluid_Lattice class, 
// is called by initialize_fluid_lattice function in lattice.cc
//
// at the moment the constructor does not do much, just defines some
// basic parameters
//
// reads in the lattice size that is passed on by the solid lattice
// = amount of particles along X, this is also the width of the 
// repulsion box within the initiial configuration. 
// Careful that repulsion box width in the solid is twice X in order
// to leave space on the right hand side for extension. 
// ---------------------------------------------------------------

Fluid_Lattice::Fluid_Lattice (int lattice_size):

	// -----------------------------------------------------------------------------
    // default variable values. Variables are declared in Pressure_lattice header
    // -----------------------------------------------------------------------------
    
   dim(100),  // the default grid width, but this is read in from the lattice directly
	
    box(1)    // ? 
{
	pl_size = lattice_size; // read in the actual lattice size from the solid (amount of particles in x)
	
	dim = lattice_size / 2; // fluid lattice size (dim) is half of lattice size
	
	width = 1.0/dim;  // width of fluid lattic box, non-dimensional
	
	time_a = 1; // in sec, one day is 86400, default time 1 second

	mu = 1.0e-03; 	// viscosity default
	
	compressibility = 4.5e-10;		// water compressibility = 4.5e10 m.m/N at 25 C°, default

	compressibility_c = -1;
	compressibility_T = 1;
	
	rho_fluid = 1.0e+03;				// water mass density (1000 kg/m³), default
	
	scale_constt = 100;               // box size in meters, default, is changed later on
	 
}

//--------------------------------------------------------------------------------------------
// Boundary and initialization conditions, functions that build up the initial config. 
//--------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------
// this function just sets the background pressure for the fluid lattice in cases where there
// is no initial gradient. 
//
// two fluid pressure matrices to calculate delta P in times
//
// called from lattice (void Lattice::Initialize_Fluid_Lattice(double pressure, double scale))
//--------------------------------------------------------------------------------------------

void Fluid_Lattice::Background_Pressure(double pressure, double concentration)
{
	int i,j;
	
	for (i=0;i<dim;i++)
	{
		for (j=0;j<dim;j++)
		{
			oldPf[i][j] = pressure;
			Pf[i][j] = pressure;
			Con[i][j] = concentration;
			Metal_A[i][j] = concentration;
			Temperature[i][j] = concentration;
			fracture[i][j] = 0.0; // no initial fractures
		}
	}
}

// this was just for debugging, maybe remove

void Fluid_Lattice::Background_Concentration(double concentration)
{
	int i,j;
	
	for (i=0;i<dim;i++)
	{
		for (j=0;j<dim;j++)
		{
			Con[i][j] = concentration;
		}
	}
}

void Fluid_Lattice::Set_Concentration(int x, int y, double concentration)
{
	Con[x][y] = concentration;
}


//---------------------------------------------------------------------------------------------
// Setting the scale (and non-dimensional area of node)
//
// now also includes a change in time so that this has not to be changed in fluid lattice
//
// called from lattice (void Lattice::Initialize_Fluid_Lattice(double pressure, double scale))
//---------------------------------------------------------------------------------------------

void Fluid_Lattice::Set_Scale(double scale, double time_factor)
{
	scale_constt = scale;
	area_node = width * width;
	time_a = time_a * time_factor;
}

//--------------------------------------------------------------------------------------------------
// setting a boundary condition at the start with hydrostatic conditions within the box
//--------------------------------------------------------------------------------------------------

void Fluid_Lattice::Set_hydrostatic_gradient(int depth)
{
	int i,j;
	
	for(i = 0; i < dim; i++)
	{	
		for(j = 0; j < dim; j++)
		{
		
			
			Pf[i][j] = (depth+(scale_constt*(float((dim-1)-j)/(dim-1))))*1000*9.81;
		
		}
	}
	
}


//------------------------------------------------------------------------------------------------
// Sending information to the fluid lattice, need porosity or solid fraction per node and the solid
// velocity. 
// 
// function receives the particle list, which is a pointer to the repulsion box, so that we can find
// the particles easily. Each of the boxes contains pointers to the actual particle/particles in the
// box, so that parameters in the particle can be easily accessed. 
//
// fracture effect not yet used
//
// The solid fraction is determined by summing up the radii of particles, but the fluid radii. Particles
// have a radius for the elastic part and a DIFFERENT radius for the fluid representing the "area" of the
// particle 
//
// called from lattice: void Lattice::Fluid_Parameters()
//-------------------------------------------------------------------------------------------------


void Fluid_Lattice::Read_Porosity_Movement_Matrix(double fracture_effect, Particle **list, int boundary)
{
	int i,j,k, particle_in_box, count;
	double solid_average, vel_x_av, vel_y_av;

// loop through the matrix, i is x, and j is y (note that Irfan did the opposite in pressure lattice)
// Each fluid box contains 4 repulsion boxes. 

// calculate an average for the boundary conditions. These boundary conditions turn out to be a major pain
// there are three different ones at the moment, the average is number 3
    
	solid_average = 0.0;
	vel_x_av = 0.0;
	vel_y_av = 0.0;
	count = 0;

	for (i = 1; i < dim-1; i++) // loop in x = i and y = j except for boundaries
	{
		for (j = 1; j < dim-1; j++)
		{	
			
			particle_in_box = 0;	// count sum of particles in the box (FLUID box)
			rho = 0;				// solid fraction sum up
						
			vel_x[i][j] = 0;		// velocity x matrix
			vel_y[i][j] = 0;		// velocity y matrix
			
			solid_fraction[i][j] = 0.9;		// solid fraction matrix	
			
			//------------------------------------------------------------------------------------------------
			// In order to get the right solid fraction and to avoid grid effects converting a triangular
			// solid lattice into the square fluid lattice we use a smoothing function and consider solid
			// in the four repulsion boxes of the fluid node plus the surrounding 12 repulsion boxes.  				
			// The solid is then weight using the smoothing function depending on how far the particle is 				 
			// from the centre of the pressure node. 
			//
			// this is also applied to the velocities. 
			//-------------------------------------------------------------------------------------------------
			
			//-------------------------------------------------------------------------------------------------
			// Because of the above the outer rows of the matrix have to be treated separately, otherwise 
			// we look into boxes that dont exist. 
			//-------------------------------------------------------------------------------------------------
			
			//if((i > 0 && i < dim-1) && (j > 0 && j < dim-1))
			{		
				//--------------------------------------------------------------------------------------------
				// Now define counters for all 16 repulsion boxes that we have to look into to define the 
				// solid fraction for the pressure node as a function of i and j. 
				// start in the lower left hand corner with k = 0, then go to the right. So 0,1,2,3 are below 
				// the actual pressure node, 5,6,9,10 are in the actual pressure node. 
				//
				// the repulsion box is a one-dimensional list starting in the lower left hand corner of the 
				// lattice with 0 and running towards the right with TWICE the lattice width (so for a 100 
				// lattice up to 199). The box in the second horizontal row above 0 is then 200 and so on.
                //
                // at the moment this routine is not using the boundaries because the boxes there are
                // either not completely full or are empty. There is already the try to double up full boxes
                // for the boundaries, but that does not work yet, needs further debugging
				//--------------------------------------------------------------------------------------------
				
					for (k = 0; k < 16; k++) 
					{
						if (k == 0)
						{
							box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // pl_size = resolution of particle lattice
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2); // take 2
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size))+ 4*pl_size +1+((i-1)*2); // take 8
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10
							
								
						}
						else if (k == 1)
						{
							box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2);
							if (i==0)
								box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2); // take 3
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
							
								
						}
						else if (k == 2)
						{
							box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // take 0
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
							
								
						}
						else if (k == 3)
						{
							box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2); // take 1
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2); // take 11	
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							
						}
							
						else if (k == 4)
						{
							box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2); // take 6
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
					
							
						}
						else if (k == 5)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +4+((i-1)*2); // take 7
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2); // take 13
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10									
						}
						else if (k == 6)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2); // take 4
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
						
								
						}
						else if (k == 7)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2); // take 15
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
						
								
						}	
						else if (k == 8)
						{
							box = (((j*2)-1)*(2*pl_size))+ 4*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // take 0
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
								
						}
						else if (k == 9)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2); // take 11
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2); // take 1
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
								
						}
						else if (k == 10)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +1+((i-1)*2); // take 8
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2); // take 2
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
							
								
						}
						else if (k == 11)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2); // take 3
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
								
						}	
						else if (k == 12)
						{
							box = (((j*2)-1)*(2*pl_size))+ 6*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2); // take 4
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
														
								
						}
						else if (k == 13)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2); // take 15
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
						
								
						}
						else if (k == 14)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +1+((i-1)*2); // take 12
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
							
								
						}
						else if (k == 15)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2); // take 13
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +4+((i-1)*2); // take 7
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							
								
						}
						
						//-----------------------------------------------------------
						// If the box is filled deal with the particle 
						//-----------------------------------------------------------
						
						if(list[box])
						{
							help = list[box]; // help now points at particle 
							
							//------------------------------------------------------------------------------
							// first the smoothening function, get distance from centre node to particle 
							// in x and y
							//------------------------------------------------------------------------------
							
							x_diff = abs(help->xpos - ((double(i)*width)+ (width/2.0)));
							y_diff = abs(help->ypos - ((double(j)*width)+ (width/2.0)));
								
							// adjust if too small
									
							if(y_diff<(help->radius/2.0))
								y_diff = help->radius/2.0;
								
							if(x_diff<(help->radius/2.0))
								x_diff = help->radius/2.0;
										
							// should not be larger than the width
													
							if (x_diff > width)
								x_diff = width;
							if (y_diff > width)
								y_diff = width;
							
							// final smooth function
							
							smooth_func = (1.0-(x_diff/width))*(1.0-(y_diff/width));
							
							
							// add up solid fraction
	
							rho = rho + smooth_func*help->area_par_fluid;
									
							// add up velocities
																
							vel_x[i][j] = vel_x[i][j] + (help->velx * smooth_func);				// velocity (flux) per node
							vel_y[i][j] = vel_y[i][j] + (help->vely * smooth_func);
							
							if(fracture_effect>0.0)
							{
								if(help->is_boundary)
								{
									fracture[i][j] = fracture_effect;
								}
							}
													
							// count particles
														
							particle_in_box ++;
							
							//-----------------------------------------------------------------------
							// check if there is an additional particle in the repulsion box
							// the additional particle would be attached to the help particle with 
							// the next_inBox pointer. 
							//-----------------------------------------------------------------------
							
							while (help->next_inBox)
							{
								// and do the same stuff as above. 
								
								x_diff = abs(help->next_inBox->xpos - ((double(i)*width)+ (width/2.0)));
								y_diff = abs(help->next_inBox->ypos - ((double(j)*width)+ (width/2.0)));
								
								
								if(y_diff<(help->radius/2.0))
									y_diff = help->radius/2.0;
								if(x_diff<(help->radius/2.0))
									x_diff = help->radius/2.0;
								
								if (x_diff > width)
									x_diff = width;
								if (y_diff > width)
									y_diff = width;
						
								smooth_func = (1.0-(x_diff/width))*(1.0-(y_diff/width));														
				
								rho = rho + smooth_func*help->next_inBox->area_par_fluid;
							
								vel_x[i][j] = vel_x[i][j] + (help->next_inBox->velx * smooth_func);
								vel_y[i][j] = vel_y[i][j] + (help->next_inBox->vely * smooth_func);
								
								if(fracture_effect>0.0)
								{
									if(help->is_boundary)
									{
										fracture[i][j] = fracture_effect;
									}
								}
								
								particle_in_box ++;
																			
								help = help->next_inBox;
							}							
						}	
					}
				
				// calculate solid fraction
				
					solid_fraction[i][j] = rho/area_node;
					
					solid_average = solid_average + solid_fraction[i][j]; // for boundary condition
					count ++;
															
					if (solid_fraction[i][j] >= 1.0) // cannot be more than 1
						solid_fraction[i][j] = 0.999;
						
					if (solid_fraction[i][j] <= 0.01) // not too small, might check
						solid_fraction[i][j] = 0.01;
					
					vel_x[i][j] = vel_x[i][j]/rho;				// average velocity(flux) per node
					vel_y[i][j] = vel_y[i][j]/rho;		
					
					vel_x_av += vel_x[i][j]; // for boundary condition
					vel_y_av +=	vel_y[i][j];	
			}			
		}
	}

	if (boundary == 1)
	{
		// boundary conditions, apply after matrix filled, 
		// condition 1: simply copy the next row from the matrix
		// to the boundary row on sides as well as at bottom and 
		// top. Works with random background noise. Becomes
		// problematic when Elle grains are used (boundaries then
		// seem to slow things down because they are mainly oriented 90 degrees
		// to the boundary. 
		// condition 2: copy neighbour row to boundary row for upper and lower boundary
		// and wrap rows for left and right boundary. Again can be problematic when 
		// grain boundaries are used, but does help avoid "doubling" effects during dynamic
		// permeability development
		// condition 3: take an average of the whole lattice and apply that to boundaries. 
		// Needs testing. 
		
		for(j = 1; j < dim-1; j++) //copy neighbour row
		{
			solid_fraction[0][j] = solid_fraction[1][j];
			vel_x[0][j] = vel_x[1][j];
			vel_y[0][j] = vel_y[1][j];
		
			solid_fraction[dim-1][j] = solid_fraction[dim-2][j];
			vel_x[dim-1][j] = vel_x[dim-2][j];
			vel_y[dim-1][j] = vel_y[dim-2][j];		
				
		}
		
		for(i = 0; i < dim; i++) //copy neighbour row
		{
			solid_fraction[i][0] = solid_fraction[i][1];
			vel_x[i][0] = vel_x[i][1];
			vel_y[i][0] = vel_y[i][1];
		
			solid_fraction[i][dim-1] = solid_fraction[i][dim-2];
			vel_x[i][dim-1] = vel_x[i][dim-2];
			vel_y[i][dim-1] = vel_y[i][dim-2];
		}	
	}
	if (boundary == 2)
	{
		
		for(j = 1; j < dim-1; j++) //left and right hand side "wrap" rows
		{
			solid_fraction[0][j] = solid_fraction[dim-2][j];
			vel_x[0][j] = vel_x[dim-2][j];
			vel_y[0][j] = vel_y[dim-2][j];
		
			solid_fraction[dim-1][j] = solid_fraction[1][j];
			vel_x[dim-1][j] = vel_x[1][j];
			vel_y[dim-1][j] = vel_y[1][j];		
				
		}
		
		for(i = 0; i < dim; i++) //bottom and top copy neighbour
		{
			solid_fraction[i][0] = solid_fraction[i][1];
			vel_x[i][0] = vel_x[i][1];
			vel_y[i][0] = vel_y[i][1];
		
			solid_fraction[i][dim-1] = solid_fraction[i][dim-2];
			vel_x[i][dim-1] = vel_x[i][dim-2];
			vel_y[i][dim-1] = vel_y[i][dim-2];
		}	
	}
	if (boundary == 3) // use an average
	{
        // at the moment the average works best but needs to be multiplied
        // by a factor, otherwise the boundaries are too permeable
        // the main problem are the side boundaries if the fluid and reaction
        // travel upwards
		
		for(j = 1; j < dim-1; j++) //check this boundary condition
		{
			solid_fraction[0][j] = 1.12 * solid_average/count;
			vel_x[0][j] = vel_x_av/count;
			vel_y[0][j] = vel_y_av/count;
		
			solid_fraction[dim-1][j] = 1.12 * solid_average/count;
			vel_x[dim-1][j] = vel_x_av/count;
			vel_y[dim-1][j] = vel_y_av/count;		
				
		}
		
		for(i = 0; i < dim; i++) //check this boundary condition
		{
			solid_fraction[i][0] = solid_average/count;
			vel_x[i][0] = vel_x_av/count;
			vel_y[i][0] = vel_y_av/count;
		
			solid_fraction[i][dim-1] = solid_average/count;
			vel_x[i][dim-1] = vel_x_av/count;
			vel_y[i][dim-1] = vel_y_av/count;
		}	
	}
}

//---------------------------------------------------------------------------------
// Caculate permeability from the porosity using Karmen Cozeny relation
// The grain size is now read in directly and changes the advection a lot, because
// of the permeability
//---------------------------------------------------------------------------------

void Fluid_Lattice::Calculate_Permeability_Matrix(double Coseny_grain_size)
{
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			kappa[i][j] = 0;
			
			kappa[i][j] = (pow(Coseny_grain_size,2.0)*pow((1-solid_fraction[i][j]),3.0));
			kappa[i][j] /= 45.0*pow((solid_fraction[i][j]),2.0);
			
			if (fracture[i][j]>0.0)
			{
				kappa[i][j] += fracture[i][j];
			}
			
			if (kappa[i][j]> 0.0000001) // reset if too large, probably set too small (e11)
			{
				kappa[i][j]= 0.0000001;	
				cout << "reset permeability";			
			}
		}
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALCULATION MATRIX INVERSION
// based on implicit alternating direction method, here x and y are the same length, so only alpha needed
//
// alpha-set
//
// matrices set 1
// 
// multiplication
// 
// invert
// 
// matrices set 2
// 
// multiplication invers
// 
// Transpose
// 
// source term addition 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. Note that pressure is also in this, meaning that the 
// calucation is easily unstable because it varies so much if pressure varies.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x
// we are not using sparse matrices, this is something that should be changed, would speed up things
// this is the prefactor for each node in the grid where pf, permeability and porosity vary
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable
//--------------------------------------------------------------------------------------------------------------------

double Fluid_Lattice::alpha_set_time() // need only alpha
{
	int i,j;
	double max, correction;
	max = 0.0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;

			{
				//alpha[i][j] = (1.0 + (compressibility * Pf[i][j]))*density_factor[i][j];
				alpha[i][j] = (1.0 + (compressibility * Pf[i][j]));
				alpha[i][j] *= kappa[i][j]*time_a;
				alpha[i][j] /= (2.0*mu*compressibility*(1-solid_fraction[i][j])*pow(width*scale_constt,2.0));	
				
				//cout << alpha[i][j] << endl;
				
				if (alpha[i][j]>max)
				{
					max = alpha[i][j];
					//cout << i << ";" << j << endl;
					//cout << Pf[i][j] << endl;
					//cout << kappa[i][j] << endl;
					//cout << solid_fraction[i][j] << endl;
				}
			}
		}
	}
	cout << max << endl;
	
	correction = max/10;
	correction = time_a/correction;
	
		return correction;
}
//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. Note that pressure is also in this, meaning that the 
// calucation is easily unstable because it varies so much if pressure varies.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x
// we are not using sparse matrices, this is something that should be changed, would speed up things
// this is the prefactor for each node in the grid where pf, permeability and porosity vary
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_fluid(double nonlin_time) // need only alpha
{
	int i,j;
	
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;

			{
				//alpha[i][j] = (1.0 + (compressibility * Pf[i][j]))*density_factor[i][j];
				alpha[i][j] = (1.0 + (compressibility * Pf[i][j]));
				alpha[i][j] *= kappa[i][j]*nonlin_time;
				alpha[i][j] /= (2.0*mu*compressibility*(1-solid_fraction[i][j])*pow(width*scale_constt,2.0));	
				
				
			}
		}
	}
	
}
//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. Note that pressure is also in this, meaning that the 
// calucation is easily unstable because it varies so much if pressure varies.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x
// we are not using sparse matrices, this is something that should be changed, would speed up things
// this is the prefactor for each node in the grid where pf, permeability and porosity vary
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable
//--------------------------------------------------------------------------------------------------------------------

int Fluid_Lattice::alpha_set(int adjust) // need only alpha
{
	int i,j;
	double max;
	max = 0.0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;

			{
				//alpha[i][j] = (1.0 + (compressibility * Pf[i][j]))*density_factor[i][j];
				alpha[i][j] = (1.0 + (compressibility * Pf[i][j]));
				alpha[i][j] *= kappa[i][j]*time_a;
				alpha[i][j] /= (2.0*mu*compressibility*(1-solid_fraction[i][j])*pow(width*scale_constt,2.0));	
				
				alpha[i][j] = alpha[i][j]/adjust;
				
				if (alpha[i][j]>max)
					max = alpha[i][j];
			}
		}
	}
	cout << max << endl;
	
	if (max < 50) //was 50
		return 1;
	else
		return (int) max/50;
}

//-----------------------------------------------------------------------------------
// set the matrix for the first calculation step
// this is the matrix that could be sparse potentially
//-----------------------------------------------------------------------------------


void Fluid_Lattice::matrices_set_1()
{	
  // creats L.H.S (al[][][])  and R.H.S (bl[][][])coeffiecient matrix 
  // at first half time step of ADI method
  
  int k,j,i;
  
  for (k=0; k<dim; k++)
  {                   
    for (i=0; i<dim; i++)
    {
		for (j=0;j<dim;j++)
		{
			al[k][i][j] = 0;      
			bl[k][i][j] = 0; 
        
			if (i==j)            //for diagonal elements
			{
				if (i==0 || i==dim-1)  //set boundary conditions
				{
					al[k][i][j] = 1.0;      //column set for any given column j				
					bl[k][i][j] = 1.0;      //row set for any given row i
				}
				else
				{
					al[k][i][j] = 1+2*alpha[k][i];				
					bl[k][i][j] = 1-2*alpha[i][k];
				}
			}
			else if ((i==j-1 && i>0) || (i==j+1 && i<(dim-1)))
			{
				al[k][i][j] = -alpha[k][i];
			
				bl[k][i][j] = alpha[i][k];
			}
		}
	 }
  }    	
}

//---------------------------------------------------------------------------
// this just copies the fluid pressure matrix in order to get the change in
// time per node for the darcy velocities that are then need for the
// advection code
//----------------------------------------------------------------------------

void Fluid_Lattice::copy_matrix()
{
	int i,j;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			oldPf[i][j] = Pf[i][j];			
		}
	}
}
	

//----------------------------------------------------------------------------
// multiply diagonal matrix with the fluid pressure matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			interim_Pf[i][j] = 0;
			
			for (k = 0; k < dim; k++)
			{
				interim_Pf[i][j] += bl[j][i][k] * Pf[k][j];			
			}
		}
	}
}

//----------------------------------------------------------------------------
// Matrix inversion
//----------------------------------------------------------------------------

void Fluid_Lattice::invert()
{
  double factor;
  int g,i,j,k; 

	for (g=0; g<dim; g++)
	{
	  // Creation of identity matrix
		for (i = 0; i < dim; i++)
		{
			for (j = 0; j < dim; j++)
			{
				a_invl[g][i][j] = 0;
							
				a_invl[g][i][j] = (i==j)?1:0;			
			}
		}	
	  
	  // Development of upper-triangular matrix from the input matrix 
		// along with the subsequent changes in identity matrix.
		for (i = 0; i < dim-1; i++)
		{
			for (k = i+1; k < dim; k++)
			{
				factor = (-al[g][k][i]) / al[g][i][i];
				if (!isinf(factor) && !isnan(factor))
				{
					for (j = 0; j < dim; j++)
					{
						al[g][k][j] += al[g][i][j] * factor;
						a_invl[g][k][j] += a_invl[g][i][j] * factor;
					
					}
				}
			}
		}
		
		for(i = 0; i < dim; i++)
		{
			for(j = 0; j < dim; j ++)
			{
				if(j < i)
					al[g][i][j] = 0;
			}
		}
		
		// Development of diagonal matrix from developed upper-triangular 
		// matrix along with subsequent changes in modified identity matrix. 
		
		for (i = dim-1; i > 0; i--)
		{
			for (k = i-1; k >= 0; k--)
			{
				factor = (-al[g][k][i]) / al[g][i][i];
				if (!isinf(factor) && !isnan(factor))
				{
					for (j = 0; j < dim; j++)
					{
						al[g][k][j] += al[g][i][j] * factor;
						a_invl[g][k][j] += a_invl[g][i][j] * factor;
						
					}
				}
			}
		}
		
		for(i = 0; i < dim; i++)
		{
			for(j = 0; j < dim; j ++)
			{
				if(j > i)
					al[g][i][j] = 0;
			}
		}
		
		// Conversion of given matrix into identity matrix and on contrary
		// achievement of inverse matrix from the identity matrix.
		
		for (i = 0; i < dim; i++)
		{
			if (al[g][i][i] != 0)
			{
				factor = al[g][i][i];
				
				for (j = 0; j < dim; j++)
				{
					al[g][i][j] /= factor;
					a_invl[g][i][j] /= factor;
				}
			}
		}
	}
}

//------------------------------------------------------------------------------------------
// Set the matrix for the second step of the calculation
//------------------------------------------------------------------------------------------

void Fluid_Lattice::matrices_set_2()
{	
  // creats L.H.S (al[][][])  and R.H.S (bl[][][])coeffiecient matrix 
  // at second half time step of ADI method
  
  int k,i,j;
  
  for (k=0;k<dim;k++)
  {
	for (i=0; i<dim; i++)
	{                   
		for (j=0; j<dim; j++)
		{
			al[k][i][j] = 0;      
			bl[k][i][j] = 0; 
        
			if (i==j)            //for diagonal elements
			{
				if (i==0 || i==dim-1)  //set boundary conditions
				{
					al[k][i][j] = 1.0;      //column set for any given column j					
					bl[k][i][j] = 1.0;      //row set for any given row i
				}
				else
				{
					al[k][i][j] = 1+2*alpha[i][k];					
					bl[k][i][j] = 1-2*alpha[k][i];
				}
			}
			else if ((i==j-1 && i>0 ) || (i==j+1 && i<(dim-1)))
			{
				al[k][i][j] = -alpha[i][k];
				
				bl[k][i][j] = alpha[k][i];
			}
		}
    }
  }
}

//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Pf[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Pf[i][j] += a_invl[j][i][k] * interim_Pf[k][j];
				
		}
	}
}

//--------------------------------------------------------------------------------------
// Source term that is added to the pressure diffusion equation, uses pressure difference
// and difference in velocity of solid. 
//
// check that velocity is scaled (m/second)
//--------------------------------------------------------------------------------------

void Fluid_Lattice::test_pressure_with_source(int byo)
{	
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			source_a[i][j] = 0;
			
			if((i > 0 && i < dim-1) && (j > 0 && j < dim-1))
			{
				source_a[i][j] = ((vel_x[i][j+1]-vel_x[i][j-1])/(2.0*width*scale_constt));
				source_a[i][j] += ((vel_y[i+1][j]-vel_y[i-1][j])/(2.0*width*scale_constt));
				source_a[i][j] *= (1.0/compressibility + Pf[i][j]);
				source_a[i][j] *= time_a/(2.0*(1-solid_fraction[i][j]));
			}
		}
	}
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			source_b[i][j] = 0;
			
			if((i > 0 && i < dim-1) && (j > 0 && j < dim-1))
			{
				source_b[i][j] = vel_x[i][j]*((Pf[i][j+1]-Pf[i][j-1])/(2*width*scale_constt));
				source_b[i][j] += vel_y[i][j]*((Pf[i+1][j]-Pf[i-1][j])/(2.0*width*scale_constt));
				source_b[i][j] *= time_a/2.0;			
			}
		}
	}
	if (byo)
	{
		for(i = 0; i < dim; i++)
		{
			for(j = 0; j < dim; j++)
			{
				source_c[i][j] = 0;
			
				if((i > 0 && i < dim-1) && (j > 0 && j < dim-1))
				{
					source_c[i][j] = kappa[i][j]*time_a/(2*mu*(1-solid_fraction[i][j])*pow(width*scale_constt,2.0));	
					source_c[i][j] = 1000*source_c[i][j]*((Pf[i][j+1]-Pf[i][j-1])+((compressibility_c/compressibility)*(Con[i][j+1]-Con[i][j-1]))+((compressibility_T/compressibility)*(Temperature[i][j+1]-Temperature[i][j-1])));
				}		
			}
		}
	}
	
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			source[i][j] = 0;
			
			source[i][j] = source_a[i][j] + source_b[i][j];	
			if (byo)
					source[i][j] = source[i][j] + source_c[i][j];	
			//if (source[i][j]!=0.0)
			//cout << source[i][j] << endl;	
		}
	}
	
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
			interim_Pf[i][j] -= source[i][j];
	}
}

//------------------------------------------------------------------------------------
// Transpose the matrix (switch x and y)
//------------------------------------------------------------------------------------

void Fluid_Lattice::transpose_trans_P()
{
	double pressure_trans[dim][dim];
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			pressure_trans[i][j] = 0;
			
			pressure_trans[i][j] = interim_Pf[j][i];
		}
	}
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			interim_Pf[i][j] = 0;
			
			interim_Pf[i][j] = pressure_trans[i][j];
		}
	}		
}

//-----------------------------------------------------------------------------------------------
// concentration calculation Advective part
// at the moment an explicit forward calculation where Ci time n+1 is Ci time n minus velocity
// vector times delta t/delta x times Ci at time n minus Ci-1 at time n. Needs to determine
// the velocity vector first to find direction of Ci-1 (backward to flow)
// need to check stability, and is supposed to induce negative diffusion
//
// the prefactor is adjusted internally for the worst case scenario in the code to make it stable
// however, that means every time step is potentially different so need to adjust that internally
//------------------------------------------------------------------------------------------------

void Fluid_Lattice::SolveAdvectionLax_Wenddroff(double time, double space, int boundary)
{
	double C_for_x, C_back_x, C_for_y, C_back_y;
	int i, j, k, v, iteration;
	double dx, dy, dt;
	double prefactor, maxprefactor;
	
	dx = space/100;
	dy = space/100;
	dt = time;
	
    // this boundary condition should probably be applied externally?
    
	//for (v = 0; v < dim; v++)
	//{   
	//	Con[v][0]=0.5; // setting the lower boundary 
		//Con[v][dim-1]=0.0;
	//}
	
    // determine the largest prefactor
    
	maxprefactor = 0.0;

	for(i = 1; i < dim-1; i++) // x loop
	{
		for(j = 1; j < dim-1; j++)  // y loop
		{ 
			prefactor = fabs(vel_darcyx[i][j]*dt/dx);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
			prefactor = fabs(vel_darcyy[i][j]*dt/dy);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
		}
	}
	
	cout << "advection prefactor:" << maxprefactor << endl;
	
    // adjust prefactor to be 0.2, this should be an internal loop
    // so that the time is constant.
    
	iteration = maxprefactor/0.02;
	
	if (iteration < 1) iteration = 1;
	else dt=dt/iteration;
	
	cout << "iteration advection:" << iteration << endl;

	//step 1 calculation of half space back and forward for x and y. 

	//step 2 
	for (k = 0; k < iteration; k++)
	{
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{ 
				if (i == 0) // boundary conditions are letting fluid flow in y parallel to boundary, x term constant
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Con[i][j]+Con[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Con[i+1][j]-Con[i][j]);
						C_back_x = 0.5*(Con[i][j]+Con[dim-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Con[dim-1][j]-Con[i][j]);
			
					//step 2 x
						dxc[i][j] = Con[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Con[i][j];
				}
				else if (i == dim-1) // wrapping right boundary
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Con[i][j]+Con[0][j])-0.5*vel_darcyx[i][j]*dt/dx*(Con[0][j]-Con[i][j]);
						C_back_x = 0.5*(Con[i][j]+Con[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Con[i-1][j]-Con[i][j]);
			
					//step 2 x
						dxc[i][j] = Con[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Con[i][j];
				}
				else
				{
			
				//step 1 x
					C_for_x = 0.5*(Con[i][j]+Con[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Con[i+1][j]-Con[i][j]);
					C_back_x = 0.5*(Con[i][j]+Con[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Con[i-1][j]-Con[i][j]);
			
				//step 2 x
					dxc[i][j] = Con[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
				}
			
			//step 1 y
				C_for_y = 0.5*(Con[i][j]+Con[i][j+1])-0.5*vel_darcyy[i][j]*dt/dy*(Con[i][j+1]-Con[i][j]);
				C_back_y = 0.5*(Con[i][j]+Con[i][j-1])+0.5*vel_darcyy[i][j]*dt/dy*(Con[i][j-1]-Con[i][j]);
			
			//step 2 y
				dyc[i][j] = Con[i][j]-vel_darcyy[i][j]*dt/dy*(C_for_y-C_back_y);
			}
		}
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{
			// add up x and y
            
				Con[i][j] = 0.5*(dxc[i][j] + dyc[i][j]);
            
            // final adjustment, no overflow and no negative concentration to improve stability
            
				if (Con[i][j] > 0.5)
					Con[i][j] = 0.5;
				if (Con[i][j] < 0.0)
					Con[i][j] = 0.0;
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------
// concentration calculation Advective part
// at the moment an explicit forward calculation where Ci time n+1 is Ci time n minus velocity
// vector times delta t/delta x times Ci at time n minus Ci-1 at time n. Needs to determine
// the velocity vector first to find direction of Ci-1 (backward to flow)
// need to check stability, and is supposed to induce negative diffusion
//
// the prefactor is adjusted internally for the worst case scenario in the code to make it stable
// however, that means every time step is potentially different so need to adjust that internally
//------------------------------------------------------------------------------------------------

void Fluid_Lattice::SolveAdvectionLax_Wenddroff_Metal(double time, double space, int boundary)
{
	double C_for_x, C_back_x, C_for_y, C_back_y;
	int i, j, k, v, iteration;
	double dx, dy, dt;
	double prefactor, maxprefactor;
	
	dx = space/100;
	dy = space/100;
	dt = time;
	
    // this boundary condition should probably be applied externally?
    
	//for (v = 0; v < dim; v++)
	//{   
	//	Con[v][0]=0.5; // setting the lower boundary 
		//Con[v][dim-1]=0.0;
	//}
	
    // determine the largest prefactor
    
	maxprefactor = 0.0;

	for(i = 1; i < dim-1; i++) // x loop
	{
		for(j = 1; j < dim-1; j++)  // y loop
		{ 
			prefactor = fabs(vel_darcyx[i][j]*dt/dx);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
			prefactor = fabs(vel_darcyy[i][j]*dt/dy);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
		}
	}
	
	cout << "advection prefactor:" << maxprefactor << endl;
	
    // adjust prefactor to be 0.2, this should be an internal loop
    // so that the time is constant.
    
	iteration = maxprefactor/0.02;
	
	if (iteration < 1) iteration = 1;
	else dt=dt/iteration;
	
	cout << "iteration advection:" << iteration << endl;

	//step 1 calculation of half space back and forward for x and y. 

	//step 2 
	for (k = 0; k < iteration; k++)
	{
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{ 
				if (i == 0) // boundary conditions are letting fluid flow in y parallel to boundary, x term constant
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Metal_A[i][j]+Metal_A[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[i+1][j]-Metal_A[i][j]);
						C_back_x = 0.5*(Metal_A[i][j]+Metal_A[dim-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[dim-1][j]-Metal_A[i][j]);
			
					//step 2 x
						dxc[i][j] = Metal_A[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Metal_A[i][j];
				}
				else if (i == dim-1) // wrapping right boundary
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Metal_A[i][j]+Metal_A[0][j])-0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[0][j]-Metal_A[i][j]);
						C_back_x = 0.5*(Metal_A[i][j]+Metal_A[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[i-1][j]-Metal_A[i][j]);
			
					//step 2 x
						dxc[i][j] = Metal_A[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Metal_A[i][j];
				}
				else
				{
			
				//step 1 x
					C_for_x = 0.5*(Metal_A[i][j]+Metal_A[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[i+1][j]-Metal_A[i][j]);
					C_back_x = 0.5*(Metal_A[i][j]+Metal_A[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Metal_A[i-1][j]-Metal_A[i][j]);
			
				//step 2 x
					dxc[i][j] = Metal_A[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
				}
			
			//step 1 y
				C_for_y = 0.5*(Metal_A[i][j]+Metal_A[i][j+1])-0.5*vel_darcyy[i][j]*dt/dy*(Metal_A[i][j+1]-Metal_A[i][j]);
				C_back_y = 0.5*(Metal_A[i][j]+Metal_A[i][j-1])+0.5*vel_darcyy[i][j]*dt/dy*(Metal_A[i][j-1]-Metal_A[i][j]);
			
			//step 2 y
				dyc[i][j] = Metal_A[i][j]-vel_darcyy[i][j]*dt/dy*(C_for_y-C_back_y);
			}
		}
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{
			// add up x and y
            
				Metal_A[i][j] = 0.5*(dxc[i][j] + dyc[i][j]);
            
            // final adjustment, no overflow and no negative concentration to improve stability
            
				if (Metal_A[i][j] > 0.5)
					Metal_A[i][j] = 0.5;
				if (Metal_A[i][j] < 0.0)
					Metal_A[i][j] = 0.0;
			}
		}
	}
}


//-----------------------------------------------------------------------------------------------
// concentration calculation Advective part
// at the moment an explicit forward calculation where Ci time n+1 is Ci time n minus velocity
// vector times delta t/delta x times Ci at time n minus Ci-1 at time n. Needs to determine
// the velocity vector first to find direction of Ci-1 (backward to flow)
// need to check stability, and is supposed to induce negative diffusion
//
// the prefactor is adjusted internally for the worst case scenario in the code to make it stable
// however, that means every time step is potentially different so need to adjust that internally
//------------------------------------------------------------------------------------------------

void Fluid_Lattice::SolveAdvectionLax_Wenddroff_Temperature(double time, double space, int boundary)
{
	double C_for_x, C_back_x, C_for_y, C_back_y;
	int i, j, k, v, iteration;
	double dx, dy, dt;
	double prefactor, maxprefactor;
	
	dx = space/100;
	dy = space/100;
	dt = time;
	
    // this boundary condition should probably be applied externally?
    
	//for (v = 0; v < dim; v++)
	//{   
	//	Con[v][0]=0.5; // setting the lower boundary 
		//Con[v][dim-1]=0.0;
	//}
	
    // determine the largest prefactor
    
	maxprefactor = 0.0;

	for(i = 1; i < dim-1; i++) // x loop
	{
		for(j = 1; j < dim-1; j++)  // y loop
		{ 
			prefactor = fabs(vel_darcyx[i][j]*dt/dx);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
			prefactor = fabs(vel_darcyy[i][j]*dt/dy);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
		}
	}
	
	cout << "advection prefactor:" << maxprefactor << endl;
	
    // adjust prefactor to be 0.2, this should be an internal loop
    // so that the time is constant.
    
	iteration = maxprefactor/0.02;
	
	if (iteration < 1) iteration = 1;
	else dt=dt/iteration;
	
	cout << "iteration advection:" << iteration << endl;

	//step 1 calculation of half space back and forward for x and y. 

	//step 2 
	for (k = 0; k < iteration; k++)
	{
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{ 
				if (i == 0) // boundary conditions are letting fluid flow in y parallel to boundary, x term constant
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Temperature[i][j]+Temperature[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Temperature[i+1][j]-Temperature[i][j]);
						C_back_x = 0.5*(Temperature[i][j]+Temperature[dim-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Temperature[dim-1][j]-Temperature[i][j]);
			
					//step 2 x
						dxc[i][j] = Temperature[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Temperature[i][j];
				}
				else if (i == dim-1) // wrapping right boundary
				{
					if (boundary == 1)
					{
						C_for_x = 0.5*(Temperature[i][j]+Temperature[0][j])-0.5*vel_darcyx[i][j]*dt/dx*(Temperature[0][j]-Temperature[i][j]);
						C_back_x = 0.5*(Temperature[i][j]+Temperature[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Temperature[i-1][j]-Temperature[i][j]);
			
					//step 2 x
						dxc[i][j] = Temperature[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
					}
					if (boundary == 2) // 
						dxc[i][j] = Temperature[i][j];
				}
				else
				{
			
				//step 1 x
					C_for_x = 0.5*(Temperature[i][j]+Temperature[i+1][j])-0.5*vel_darcyx[i][j]*dt/dx*(Temperature[i+1][j]-Temperature[i][j]);
					C_back_x = 0.5*(Temperature[i][j]+Temperature[i-1][j])+0.5*vel_darcyx[i][j]*dt/dx*(Temperature[i-1][j]-Temperature[i][j]);
			
				//step 2 x
					dxc[i][j] = Temperature[i][j]-vel_darcyx[i][j]*dt/dx*(C_for_x-C_back_x);
				}
			
			//step 1 y
				C_for_y = 0.5*(Temperature[i][j]+Temperature[i][j+1])-0.5*vel_darcyy[i][j]*dt/dy*(Temperature[i][j+1]-Temperature[i][j]);
				C_back_y = 0.5*(Temperature[i][j]+Temperature[i][j-1])+0.5*vel_darcyy[i][j]*dt/dy*(Temperature[i][j-1]-Temperature[i][j]);
			
			//step 2 y
				dyc[i][j] = Temperature[i][j]-vel_darcyy[i][j]*dt/dy*(C_for_y-C_back_y);
			}
		}
		for(i = 0; i < dim; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{
			// add up x and y
            
				Temperature[i][j] = 0.5*(dxc[i][j] + dyc[i][j]);
            
            // final adjustment, no overflow and no negative concentration to improve stability
            
				if (Temperature[i][j] > 0.5)
					Temperature[i][j] = 0.5;
				if (Temperature[i][j] < 0.0)
					Temperature[i][j] = 0.0;
			}
		}
	}
}


//-------------------------------------------------------------------------------------------------------
// this is not used at the moment, but can potentially be used to change concentration after a reaction
//
// sept 2018
//--------------------------------------------------------------------------------------------------------

void Fluid_Lattice::ChangeConcentration(int box_x, int box_y, double change)
{
	Con[box_y][box_x] = Con[box_y][box_x] - change;
	if (Con[box_y][box_x]< 0.0)
		Con[box_y][box_x]= 0.0;
}



//------------------------------------------------------------------------------------------
// Passing back the values to the solid lattice including the fluid pressure gradients as
// force in x and y that is added to the force ballance in relaxation, but not to the 
// calculated stress, so that the stress remains to be the solid stress only. 
// 
// This takes each pressure node and looks into its 4 repulsion boxes and gives the values to 
// the particles in there.
//
// can potentially calculate average concetration between concentration nodes, but this
// does not seem to work well at the moment sept 2018
//
// The pressure gradient is taken from the particle repulsion box to the neighbouring node
//-------------------------------------------------------------------------------------------

void Fluid_Lattice::Pass_Back_Gradients(Particle **list, int av_conc)
{
	int i,j,k;
	Particle *help;
	double smooth, grad_x, grad_y, conc_av_x, conc_av_y;
	
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			//--------------------------------------------------------------------
			// 0 is lower left box, 1 lower right box, 2 upper left box
			// and 3 upper right box. 
			//
			// we need special boundary conditions for the boundary boxes
			// left and right can be wrappinp, but up and down may be more 
			// problematic
			//
			// at the moment right and left wrap, up and down copy next row
			//--------------------------------------------------------------------
			
			for (k = 0; k < 4; k++) // go to four quadrants of pressure node
			{
				if (k == 0)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
				else if (k == 1)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
				else if (k == 2)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
				else if (k == 3)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
				
				if(list[box])
				{
					help = list[box];
					x_diff = abs(help->xpos - ((double(i)*width)+ (width/2.0)));
					y_diff = abs(help->ypos - ((double(j)*width)+ (width/2.0)));
					smooth = (1-(x_diff/width))*(1-(y_diff/width));
					
					if (k==0) // lower left box
					{
						if (i == 0) // left boundary, deal with x
						{
							grad_x =  Pf[dim-1][j] - Pf[i][j];	// wrap	
							conc_av_x = (Con[dim-1][j] + Con[i][j])/2.0;
						}
						else // normal
						{
							grad_x =  Pf[i-1][j] - Pf[i][j];
							conc_av_x = (Con[dim-1][j] + Con[i][j])/2.0;
						}
						if (j == 0) // lower boundary, deal with y
						{
							grad_y =  Pf[i][j] - Pf[i][j+1]; // use the next row and copy that
							conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
						}
						else // normal
						{
							grad_y =  Pf[i][j-1] - Pf[i][j];
							conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
						}
					}
					else if (k==1) // lower right box
					{
						if (i == dim-1) // wrap
						{
							grad_x = Pf[i][j] - Pf[0][j];
							conc_av_x = (Con[i][j] + Con[0][j])/2.0;
						}
						else // normal
						{
							grad_x = Pf[i][j] - Pf[i+1][j];	
							conc_av_x = (Con[i][j] + Con[i+1][j])/2.0;		
						}
						if (j == 0)
						{
							grad_y = Pf[i][j] - Pf[i][j+1];
							conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
						}
						else
						{
							grad_y = Pf[i][j-1] - Pf[i][j];
							conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
						}
					}
					else if (k==2)
					{
						if (i == 0) // left boundary
						{
							grad_x =  Pf[dim-1][j] - Pf[i][j];
							conc_av_x = (Con[dim-1][j] + Con[i][j])/2.0;						
						}
						else
						{
							grad_x = Pf[i-1][j] - Pf[i][j];
							conc_av_x = (Con[i-1][j] + Con[i][j])/2.0;
						}
						if (j == dim-1)
						{
							grad_y =  Pf[i][j-1] - Pf[i][j];
							conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
						}
						else
						{
							grad_y =  Pf[i][j] - Pf[i][j+1];
							conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
						}
						
					}
					else if (k==3)
					{
						if (i == dim-1)
						{
							grad_x = Pf[i][j] - Pf[0][j];
							conc_av_x = (Con[i][j] + Con[0][j])/2.0;
						} 
						else 
						{
							grad_x = Pf[i][j] - Pf[i+1][j];
							conc_av_x = (Con[i][j] + Con[i+1][j])/2.0;
						}
						if (j == dim-1)
						{
							grad_y =  Pf[i][j-1] - Pf[i][j];
							conc_av_x = (Con[i][j-1] + Con[i][j])/2.0;
						}
						else
						{
							grad_y =  Pf[i][j] - Pf[i][j+1];
							conc_av_x = (Con[i][j] + Con[i][j+1])/2.0;
						}
					}
                    
                    // pass the pressure gradient to each particle as force in x and y
                    
					help->F_P_y = grad_y  * 3.14 * help->real_radius * help->real_radius/solid_fraction[i][j];
					help->F_P_x = grad_x  * 3.14 * help->real_radius * help->real_radius/solid_fraction[i][j];
					help->F_P_y = help->F_P_y / (width*scale_constt);
					help->F_P_x = help->F_P_x / (width*scale_constt);
								
                    // pass the darcy velocities in x and y. note that they are calculated in a differnt
                    // function
                    
					help->v_darcyx = vel_darcyx[i][j];
					help->v_darcyy = vel_darcyy[i][j];
                    
                    // pass aditional values for visualization purposes to the particles
                    
					help->temperature = Pf[i][j];
					help->fluid_pressure_gradient = help->F_P_y;
					help->average_porosity = 1-solid_fraction[i][j];
					
                    // use or dont use the average concentration
                    
					if (av_conc)					
						help->conc = (conc_av_x + conc_av_y)/2.0 ;
					else 
						help->conc = Con[i][j];
					
					help->metal_conc = Metal_A[i][j];
						
					while(help->next_inBox) // and do the whole thing for other particles in the box
					{						
						help = help->next_inBox;

						x_diff = abs(help->xpos - ((double(i)*width)+ (width/2.0)));
						y_diff = abs(help->ypos - ((double(j)*width)+ (width/2.0)));
						smooth = (1-(x_diff/width))*(1-(y_diff/width));
					
						if (k==0) // lower left box
						{
							if (i == 0) // left boundary, deal with x
							{
								grad_x =  Pf[dim-1][j] - Pf[i][j];	// wrap		
								conc_av_x = (Con[dim-1][j] + Con[i][j])/2.0;
							}
							else // normal
							{
								grad_x =  Pf[i-1][j] - Pf[i][j];
								conc_av_x = (Con[i-1][j] + Con[i][j])/2.0;
							}
							if (j == 0) // lower boundary, deal with y
							{
								grad_y =  Pf[i][j] - Pf[i][j+1]; // use the next row and copy that 
								conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
							}
							else // normal
							{
								grad_y =  Pf[i][j-1] - Pf[i][j];
								conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
							}
						}
						else if (k==1) // lower right box
						{
							if (i == dim-1) // wrap
							{
								grad_x = Pf[i][j] - Pf[0][j];
								conc_av_x = (Con[i][j] + Con[0][j])/2.0;
							}
							else // normal
							{
								grad_x = Pf[i][j] - Pf[i+1][j];	
								conc_av_x = (Con[i][j] + Con[i+1][j])/2.0;
							}
							if (j == 0)
							{
								grad_y = Pf[i][j] - Pf[i][j+1];
								conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
							}
							else
							{
								grad_y = Pf[i][j-1] - Pf[i][j];
								conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
							}
						}
						else if (k==2)
						{
							if (i == 0) // left boundary
							{
								grad_x =  Pf[dim-1][j] - Pf[i][j];	
								conc_av_x = (Con[dim-1][j] + Con[i][j])/2.0;
							}
							else
							{
								grad_x = Pf[i-1][j] - Pf[i][j];
								conc_av_x = (Con[i-1][j] + Con[i][j])/2.0;
							}
							if (j == dim-1)
							{
								grad_y =  Pf[i][j-1] - Pf[i][j];
								conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
							}
							else
							{
								grad_y =  Pf[i][j] - Pf[i][j+1];
								conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
							}
						
						}
						else if (k==3)
						{
							if (i == dim-1)
							{
								grad_x = Pf[i][j] - Pf[0][j];
								conc_av_x = (Con[i][j] + Con[0][j])/2.0;
							}
							else 
							{
								grad_x = Pf[i][j] - Pf[i+1][j];
								conc_av_x = (Con[i][j] + Con[i+1][j])/2.0;
							}
							if (j == dim-1)
							{
								grad_y =  Pf[i][j-1] - Pf[i][j];
								conc_av_y = (Con[i][j-1] + Con[i][j])/2.0;
							}
							else
							{
								grad_y =  Pf[i][j] - Pf[i][j+1];
								conc_av_y = (Con[i][j] + Con[i][j+1])/2.0;
							}
						}
						
						// fluid forces from pressure gradients for lattice
						
						help->F_P_y = grad_y  * 3.14 * help->real_radius * help->real_radius/solid_fraction[i][j];
						help->F_P_x = grad_x  * 3.14 * help->real_radius * help->real_radius/solid_fraction[i][j];
						help->F_P_y = help->F_P_y / (width*scale_constt);
						help->F_P_x = help->F_P_x / (width*scale_constt);
						
						// output values for visualization 
						
						help->v_darcyx = vel_darcyx[i][j];
						help->v_darcyy = vel_darcyy[i][j];																				
						help->temperature = Pf[i][j];
						help->fluid_pressure_gradient = help->F_P_y;
						help->average_porosity = 1-solid_fraction[i][j];
						
						if (av_conc)
							help->conc = (conc_av_x + conc_av_y)/2.0;
						else
							help->conc = Con[i][j] ;
						
						help->metal_conc = Metal_A[i][j];
						
					}				
				}
						
			}
		}
	}
	
}

//--------------------------------------------------------------------------------------------------------------
// This just sets some boundaries for the fluid lattice plus potentially one for the sides, but that is
// not necessarily need because the IDE method using parallel flow along the boundaries already
//--------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::Set_Boundaries(double UpperBoundaryPressure, double LowerBoundaryPressure, int double_sides, double conc, double additional)
{
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		if (UpperBoundaryPressure!=0.0)	
		{
			if (i>0 & i<100)
			{
				Pf[i][dim-1] = UpperBoundaryPressure + additional;
				Con[i][dim-1] = conc;
				Con[i][dim-2] = conc;
				Metal_A[i][dim-1] = conc;
			
			}
			else
				Pf[i][dim-1] = UpperBoundaryPressure;
		}
				
		if (LowerBoundaryPressure!=0.0)	
			
		{
			if (i>0 & i<100)
			{
				Pf[i][0] = LowerBoundaryPressure + additional;
				Metal_A[i][0] = conc;
				Con[i][0] = conc;
				Con[i][1] = conc;
				Temperature[i][0] = conc;
			}
			else
				Pf[i][0] = Pf[i][1];
		}
		if (double_sides)
		{
			if (i>0 & i<150)
			{
				Pf[0][i] = LowerBoundaryPressure + additional;
				Con[0][i] = conc;
				Con[1][i] = conc;
				Metal_A[0][i] = conc;
			}
			else 
				Pf[0][i] =Pf[1][i];
			//Pf[dim-1][i] = UpperBoundaryPressure + additional;
			//Metal_A[dim-1][i] = conc
		
			
			if (i>0 & i<100)
			{
				Pf[dim-1][i] = LowerBoundaryPressure + additional;
				Con[dim-1][i] = conc;
				Con[dim-2][i] = conc;
				Metal_A[dim-1][i] = conc;
			}
			else 
			
			Pf[dim-1][i] =Pf[dim-2][i];
		}
	}
	
}


//-------------------------------------------------------------------------------------------
// simple increase of pressure at a single node
//-------------------------------------------------------------------------------------------

void Fluid_Lattice::Fluid_Input(double pressure,int x,int y)
{
	Pf[x][y] =  Pf[x][y] + pressure;
	Con[x][y] += 0.5;
}

//-------------------------------------------------------------------------------------------
// this is the core for the advection code, gets the darcy velocity
// at the moment a function of changes in pressure in the pressure diffusion code.
// that means the concentration travels as a function of pressure changes for the advection
//
// first get delta P and then the unit vectors for old grad in x and y for the direction of flow.
//
// This could potentially be adjusted but works well at the moment sept 2018
//-------------------------------------------------------------------------------------------

void Fluid_Lattice::Get_Fluid_Velocity()
{
	int i, j; 
	double grad_xold, grad_yold, unit_vector; 
	double deltaPf;
	
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
            // get delta P, Pf change per time step
            
			deltaPf = Pf[i][j] - oldPf[i][j];
			
			if (i == 0) // flow parallel to left and right hand boundaries
			{
				if (j == 0)
				{
					grad_xold = 0.0;
					grad_yold = 1.0;
				}
				else if (j == dim-1)
				{
					grad_xold = 0.0;
					grad_yold = 1.0;
				}
				else
				{
					grad_xold = 0.0;
					grad_yold = oldPf[i][j-1] - oldPf[i][j+1];
				}
			}
			else if (i == dim-1) //right boundary wrap
			{
				if (j == 0)
				{
					grad_xold = 0.0;
					grad_yold = 1.0;
				}
				else if (j == dim-1)
				{
					grad_xold = 0.0;
					grad_yold = 1.0;
				}
				else
				{
					grad_xold = 0.0;
					grad_yold = oldPf[i][j-1] - oldPf[i][j+1];
				}
			}
			else if (j==0) //bottom static only upwards
			{
				grad_xold = 0.0;
				grad_yold = 1.0;
			}
			else if (j==dim-1) //top static only upwards
			{
				grad_xold = 0.0;
				grad_yold = 1.0;
			}
			else //normal case
			{
				grad_xold = oldPf[i-1][j] - oldPf[i+1][j];
				grad_yold = oldPf[i][j-1] - oldPf[i][j+1];
			}
            // get the unit vectors for x and y
            
			unit_vector = sqrt((grad_xold*grad_xold)+(grad_yold*grad_yold));
					
			if (unit_vector!=0.0)
			{
				grad_xold = grad_xold/unit_vector;
				grad_yold = grad_yold/unit_vector;
			}
			else 
			{
				grad_xold = 0.0;
				grad_yold = 0.0;
			}
			
            // and finally calculate the darcy velocity in x and y
            
			vel_darcyx[i][j] = grad_xold * deltaPf * kappa[i][j]/(mu*(1-solid_fraction[i][j])*scale_constt*width);
			vel_darcyy[i][j] = grad_yold * deltaPf * kappa[i][j]/(mu*(1-solid_fraction[i][j])*scale_constt*width);
			
			//if (vel_darcyx[i][j] > 0.0000000001)
			//	cout << vel_darcyx[i][j] << endl;
			
			//vel_darcyx[i][j] = grad_xold * deltaPf /((1-solid_fraction[i][j])*scale_constt*width);
			//vel_darcyy[i][j] = grad_yold * deltaPf /((1-solid_fraction[i][j])*scale_constt*width);
			
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALCULATION MATRIX INVERSION for the concentration, simpler than pressure, does not include source term
//
// based on implicit alternating direction method, here x and y are the same length, so only alpha needed
//
// alpha-set
//
// matrices set 1
//
// multiplication
//
// invert
//
// matrices set 2
//
// multiplication invers
//
// Transpose
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x.
// We are not using sparse matrices, this is something that should be changed, would speed up things.
// This is the prefactor for each node in the grid with a constant diffusion coefficient for the moment. 
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable.
//--------------------------------------------------------------------------------------------------------------------


void Fluid_Lattice::SolveDiffusionImplicit(double dif_const, double time, double space)
{
	alpha_set_dif(dif_const, time);
				
	matrices_set_1();
	multiplication_dif();
	transpose_trans_dif();
	invert();			
	multiplication_inv_dif();
					
	matrices_set_2();
	multiplication_dif();
	transpose_trans_dif();
	invert();
	multiplication_inv_dif();
    
}


//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. Note that pressure is also in this, meaning that the 
// calucation is easily unstable because it varies so much if pressure varies.
//
// this difusion constant is constant at the moment, but could be variable depending on porosity/permeability of nodes
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_dif(double dif_constant, double time_dif) // need only alpha
{
	int i,j;
	double max;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;
			{
				alpha[i][j] = time_dif*dif_constant/ pow(width*scale_constt,2.0);
				if (alpha[i][j]>max) max = alpha[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}


//----------------------------------------------------------------------------
// multiply diagonal matrix with the fluid pressure matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_dif()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Con[k][j];			
			}
		}
	}
}


//------------------------------------------------------------------------------------
// Transpose the matrix (switch x and y)
//------------------------------------------------------------------------------------

void Fluid_Lattice::transpose_trans_dif()
{
	double con_trans[dim][dim];
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			con_trans[i][j] = 0;
			
			con_trans[i][j] = ddyc[j][i];
		}
	}
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			ddyc[i][j] = 0;
			
			ddyc[i][j] = con_trans[i][j];
		}
	}		
}


//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_dif()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Con[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Con[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALCULATION MATRIX INVERSION for the concentration of Metals, simpler than pressure, does not include source term
//
// based on implicit alternating direction method, here x and y are the same length, so only alpha needed
//
// alpha-set
//
// matrices set 1
//
// multiplication
//
// invert
//
// matrices set 2
//
// multiplication invers
//
// Transpose
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x.
// We are not using sparse matrices, this is something that should be changed, would speed up things.
// This is the prefactor for each node in the grid with a constant diffusion coefficient for the moment. 
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable.
//--------------------------------------------------------------------------------------------------------------------


void Fluid_Lattice::SolveMetalDiffusionImplicit(double dif_const, double time, double space)
{
	alpha_set_met(dif_const, time);
				
	matrices_set_1();
	multiplication_met();
	transpose_trans_met();
	invert();			
	multiplication_inv_met();
					
	matrices_set_2();
	multiplication_met();
	transpose_trans_met();
	invert();
	multiplication_inv_met();
    
}


//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. 
// this difusion constant is constant at the moment, but could be variable depending on porosity/permeability of nodes
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_met(double dif_constant, double time_dif) // need only alpha
{
	int i,j;
	double max;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;
			{
				alpha[i][j] = time_dif*dif_constant/ pow(width*scale_constt,2.0);
				if (alpha[i][j]>max) max = alpha[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}


//----------------------------------------------------------------------------
// multiply diagonal matrix with the fluid pressure matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_met()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Metal_A[k][j];			
			}
		}
	}
}


//------------------------------------------------------------------------------------
// Transpose the matrix (switch x and y)
//------------------------------------------------------------------------------------

void Fluid_Lattice::transpose_trans_met()
{
	double con_trans[dim][dim];
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			con_trans[i][j] = 0;
			
			con_trans[i][j] = ddyc[j][i];
		}
	}
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			ddyc[i][j] = 0;
			
			ddyc[i][j] = con_trans[i][j];
		}
	}		
}


//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_met()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Metal_A[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Metal_A[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CALCULATION MATRIX INVERSION for the Temperature, simpler than pressure, does not include source term
//
// based on implicit alternating direction method, here x and y are the same length, so only alpha needed
//
// alpha-set
//
// matrices set 1
//
// multiplication
//
// invert
//
// matrices set 2
//
// multiplication invers
//
// Transpose
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation.
// at the moment the matrix size is preset to 200 200 200 for latte simulations with 400 particles in x.
// We are not using sparse matrices, this is something that should be changed, would speed up things.
// This is the prefactor for each node in the grid with a constant diffusion coefficient for the moment. 
// this prefactor makes the implicit method potentially unstable if its too large, this should be checked and
// corrected for in a future version to make the method more stable.
//--------------------------------------------------------------------------------------------------------------------


void Fluid_Lattice::SolveTemperatureDiffusionImplicit(double dif_const, double time, double space)
{
	alpha_set_temp(dif_const, time);
				
	matrices_set_1();
	multiplication_temp();
	transpose_trans_temp();
	invert();			
	multiplication_inv_temp();
					
	matrices_set_2();
	multiplication_temp();
	transpose_trans_temp();
	invert();
	multiplication_inv_temp();
    
}


//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. Note that pressure is also in this, meaning that the 
// calucation is easily unstable because it varies so much if pressure varies.
//
// this difusion constant is constant at the moment, but could be variable depending on porosity/permeability of nodes
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_temp(double dif_constant, double time_dif) // need only alpha
{
	int i,j;
	double max;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;
			{
				alpha[i][j] = time_dif*dif_constant/ pow(width*scale_constt,2.0);
				if (alpha[i][j]>max) max = alpha[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}


//----------------------------------------------------------------------------
// multiply diagonal matrix with the fluid pressure matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_temp()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Temperature[k][j];			
			}
		}
	}
}


//------------------------------------------------------------------------------------
// Transpose the matrix (switch x and y)
//------------------------------------------------------------------------------------

void Fluid_Lattice::transpose_trans_temp()
{
	double con_trans[dim][dim];
	int i,j;
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			con_trans[i][j] = 0;
			
			con_trans[i][j] = ddyc[j][i];
		}
	}
	
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			ddyc[i][j] = 0;
			
			ddyc[i][j] = con_trans[i][j];
		}
	}		
}


//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_temp()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Temperature[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Temperature[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}




// this is an old version that did not work well and was not stable

void Fluid_Lattice::SolveadvDisExplicitTransient(double fluid_v, double dif_const)
{
    int i,j,v;
    double D,dt,dx,dy,dx2,dy2, conc_x, conc_y;
    
    D = dif_const;
    
    dx=width;//
    dy=width;//
    dx2=dx*dx;for (v = 0; v < dim; v++)
    {
        Con[v][0]=0.5; // setting the lower boundary
        Con[v][1]=0.5;
        //Con[v][dim-1]=0;
    }
    dy2=dy*dy;
    
    dt=0.002;
    
    
    // boundary condition (background concentration is already given)
    
    
    
    for(i = 0; i < dim-1; i++) // x loop
    {
        for(j = 1; j < dim-2; j++)  // y loop
        {
            
            ddyc[i][j] = ( Con[i+1][j] - 2*Con[i][j] + Con[i-1][j] )/dy2; //x has to vary in y, so takes neighbours in i (=y)
            ddxc[i][j] = ( Con[i][j+1] - 2*Con[i][j] + Con[i][j-1] )/dx2; //y has to vary in x, so takes neighbours in j (=x)
            
            // forward approximation takes the difference between node and node behind (with respect to velocity) and adds to node
            
            
            
            //if (vel_darcyx[i][j]>=0.0) // x direction positive, to the right
            if (vel_darcyx[i][j]>=0.0)
            {
                dxc[i][j]=(Con[i][j] - Con[i-1][j])/dx;
                if (dxc[i][j]>0.0)
                    dxc[i][j]=0.0;
            }
            else // x direction negative, to the left
            {
                dxc[i][j]=(Con[i][j] - Con[i+1][j])/dx;
                if (dxc[i][j]>0.0)
                    dxc[i][j]=0.0;
            }
            if (vel_darcyy[i][j]>=0.0) // y direction positive, upwards
            {
                dyc[i][j]=(Con[i][j] - Con[i][j-1])/dy;
                if (dyc[i][j]>0.0)
                    dyc[i][j]=0.0;
            }
            else // y direction positive, downwards
            {
                dyc[i][j]= (Con[i][j] - Con[i][j+1])/dy;
                if (dyc[i][j]>0.0)
                    dyc[i][j]=0.0;
            }
            
        }
    }
    
    // you now have all the values to fill in the matrix and update the concentration. You have to do that after the calculation, otherwise you get a trend in the
    // calculation depending on where you start because some have been updated and others not. Need to update in on step.
    
    for(i = 0; i < dim-1; i++) // y
    {
        for(j = 1; j < dim-2; j++) // x
        {
            // diffusion
            //Con[i][j] = Con[i][j]+dt*D*(ddxc[i][j]+ddyc[i][j]);
            //cout << " too large y" << dt*abs(vel_darcyy[i][j])*fluid_v*dyc[i][j] << endl;
            // advection
            //if (dt*abs(vel_darcyx[i][j])*fluid_v*dxc[i][j]>0.1)
            //cout << " too large x" << dt*abs(vel_darcyx[i][j])*fluid_v*dxc[i][j] << endl;
            //if (dt*abs(vel_darcyy[i][j])*fluid_v*dyc[i][j]<-0.0)
            //cout << " too large y" << dt*abs(vel_darcyy[i][j])*fluid_v*dyc[i][j] << endl;
            //else
            
            conc_x = Con[i][j]-(dt*abs(vel_darcyx[i][j])*fluid_v*dxc[i][j]);
            conc_y = Con[i][j]-(dt*abs(vel_darcyy[i][j])*fluid_v*dyc[i][j]);
            
            if (j<4)
            {
                cout << "concentration x" << conc_x << endl;
                cout << "concentration y" << conc_y << endl;
            }
            
            Con[i][j] = conc_x + conc_y;
            
            //else if (vel_darcyy[i][j]< -0.00000001)
            //{
            //    Con[i][j] = Con[i][j]-(dt*vel_darcyx[i][j]*fluid_v*dxc[i][j]);
            //    Con[i][j] = Con[i][j]-(dt*vel_darcyy[i][j]*fluid_v*dyc[i][j]);
            //}
            
            // original
            
            //concent[i][j] = concent[i][j]-(dt*1000000000000*kappa[i][j]*dyc[i][j]);
            if (Con[i][j]<0.0)
                Con[i][j]=0.0;
            else if (Con[i][j]>0.5)
                Con[i][j]=0.5;
        }
    }
    
    for (v = 1; v < dim-1; v++) // boundary condition for sides, not really wrapping... sort of
    {
        Con[0][v]=Con[1][v];
        
        Con[dim-1][v]=Con[dim-2][v];
        
    }
    
}


void Fluid_Lattice::SolveAdvectionFromm(double space, double time)
{
	int i, j, k, iteration, kk;
	double dx, dt, maxprefactor, prefactor;
	
	dx = space/100;
	dt = time;
	
	// determine the largest prefactor
    
	maxprefactor = 0.0;

	for(i = 1; i < dim-1; i++) // x loop
	{
		for(j = 1; j < dim-1; j++)  // y loop
		{ 
			prefactor = fabs(vel_darcyx[i][j]*dt/dx);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
			prefactor = fabs(vel_darcyy[i][j]*dt/dx);
			if (prefactor > maxprefactor)
				maxprefactor = prefactor;
		}
	}
	
	cout << "advection prefactor:" << maxprefactor << endl;
	
    // adjust prefactor to be 0.2, this should be an internal loop
    // so that the time is constant.
    
	iteration = maxprefactor/0.02;
	
	if (iteration < 1) iteration = 1;
	else dt=dt/iteration;
	
	cout << "iteration advection:" << iteration << endl;
	
	for (kk = 0; kk < iteration; kk++)
	{
	
		for(i = 2; i < dim-2; i++) // x loop
		{
			for(j = 2; j < dim-2; j++)  // y loop
			{ 
				dxc[i][j]=0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j])*(0.5*(Con[i+1][j]+Con[i][j])+0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+2][j]-Con[i+1][j]));
				dxc[i][j]-=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j]))*(0.25*(Con[i+1][j]+Con[i][j])-0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+2][j]-Con[i+1][j]));
				dxc[i][j]-=0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j])*(0.5*(Con[i][j]+Con[i-1][j])+0.125*(Con[i-1][j]-Con[i-2][j])-0.125*(Con[i+1][j]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j]))*(0.25*(Con[i][j]+Con[i-1][j])-0.125*(Con[i-1][j]-Con[i-2][j])-0.125*(Con[i+1][j]-Con[i][j]));
				
				dxc[i][j]+=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1])*(0.5*(Con[i][j+1]+Con[i][j])+0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+2]-Con[i][j+1]));
				dxc[i][j]-=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1]))*(0.25*(Con[i][j+1]+Con[i][j])-0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+2]-Con[i][j+1]));
				dxc[i][j]-=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1])*(0.5*(Con[i][j]+Con[i][j-1])+0.125*(Con[i][j-1]-Con[i][j-2])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1]))*(0.25*(Con[i][j]+Con[i][j-1])-0.125*(Con[i][j-1]-Con[i][j-2])-0.125*(Con[i][j+1]-Con[i][j]));
				
				dxc[i][j]*=dt/dx;
			}
		}
		
		// boundaries
		
		for(k = 1; k < dim-1; k++) // boundary loop
		{
			// 
				i = k; 
				j = 1; 
				dxc[i][j]=0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j])*(0.5*(Con[i+1][j]+Con[i][j])+0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j]))*(0.25*(Con[i+1][j]+Con[i][j])-0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j])*(0.5*(Con[i][j]+Con[i-1][j])+0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j]))*(0.25*(Con[i][j]+Con[i-1][j])-0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				
				dxc[i][j]+=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1])*(0.5*(Con[i][j+1]+Con[i][j])+0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1]))*(0.25*(Con[i][j+1]+Con[i][j])-0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1])*(0.5*(Con[i][j]+Con[i][j-1])+0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1]))*(0.25*(Con[i][j]+Con[i][j-1])-0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]*=dt/dx;
				
				i = k; 
				j = dim-2;
				dxc[i][j]=0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j])*(0.5*(Con[i+1][j]+Con[i][j])+0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j]))*(0.25*(Con[i+1][j]+Con[i][j])-0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j])*(0.5*(Con[i][j]+Con[i-1][j])+0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j]))*(0.25*(Con[i][j]+Con[i-1][j])-0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				
				dxc[i][j]+=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1])*(0.5*(Con[i][j+1]+Con[i][j])+0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1]))*(0.25*(Con[i][j+1]+Con[i][j])-0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1])*(0.5*(Con[i][j]+Con[i][j-1])+0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1]))*(0.25*(Con[i][j]+Con[i][j-1])-0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]*=dt/dx;
				
				i = 1;
				j = k;
				dxc[i][j]=0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j])*(0.5*(Con[i+1][j]+Con[i][j])+0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j]))*(0.25*(Con[i+1][j]+Con[i][j])-0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j])*(0.5*(Con[i][j]+Con[i-1][j])+0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j]))*(0.25*(Con[i][j]+Con[i-1][j])-0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				
				dxc[i][j]+=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1])*(0.5*(Con[i][j+1]+Con[i][j])+0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1]))*(0.25*(Con[i][j+1]+Con[i][j])-0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1])*(0.5*(Con[i][j]+Con[i][j-1])+0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1]))*(0.25*(Con[i][j]+Con[i][j-1])-0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]*=dt/dx;
				
				i = dim-2;
				j = k;
				dxc[i][j]=0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j])*(0.5*(Con[i+1][j]+Con[i][j])+0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i+1][j]))*(0.25*(Con[i+1][j]+Con[i][j])-0.125*(Con[i][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i+1][j]));
				dxc[i][j]-=0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j])*(0.5*(Con[i][j]+Con[i-1][j])+0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyx[i][j]+vel_darcyx[i-1][j]))*(0.25*(Con[i][j]+Con[i-1][j])-0.125*(Con[i-1][j]-Con[i-1][j])-0.125*(Con[i+1][j]-Con[i][j]));
				
				dxc[i][j]+=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1])*(0.5*(Con[i][j+1]+Con[i][j])+0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j+1]))*(0.25*(Con[i][j+1]+Con[i][j])-0.125*(Con[i][j]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j+1]));
				dxc[i][j]-=0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1])*(0.5*(Con[i][j]+Con[i][j-1])+0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]+=fabs(0.5*(vel_darcyy[i][j]+vel_darcyy[i][j-1]))*(0.25*(Con[i][j]+Con[i][j-1])-0.125*(Con[i][j-1]-Con[i][j-1])-0.125*(Con[i][j+1]-Con[i][j]));
				dxc[i][j]*=dt/dx;
				
				//dxc[0][k] = dxc[1][k];
				//dxc[dim-1][k] = dxc[dim-2][k];
		}
		
		for(i = 1; i < dim-1; i++) // x loop
		{
			for(j = 1; j < dim-1; j++)  // y loop
			{
			// add up x and y
            
				Con[i][j] = dxc[i][j];
				//cout << Con[i][j] << endl;
            
            // final adjustment, no overflow and no negative concentration to improve stability
            
				if (Con[i][j] > 0.5)
					Con[i][j] = 0.5;
				if (Con[i][j] < 0.0)
					Con[i][j] = 0.0;
			}
		}
	}	
}

//----------------------------------------------------------------------------------------------------------------
//
//		new test diffusion and exchange reaction 2021
//		Daniel and Simon 
//
//-----------------------------------------------------------------------------------------------------------------

// Initialization function 
// loop through a number of matrixes
// Mineral_Diffusion[i][j] diffusion constant for solid minerals
// Mg_conc[i][j] Mg concentration in solid 
// Fe_conc[i][j] Fe concentration in solid
// Mg_conc_b[i][j] Mg concentration in fluid 
// Fe_conc_b[i][j] Fe concentration in fluid 
// Is_Reactive_Grain[i][j] thought this was useful but not used now 
// i and j are the indexes of the matrix 
// dim is the dimension in one direction (100, 200, ... ) 

void Fluid_Lattice::Init_Mineral(double constant) 
{
	int i,j; 
	
	for(i = 0; i < dim; i++) // x loop
	{
		for(j = 0; j < dim; j++)  // y loop
		{
			Mineral_Diffusion[i][j] = constant;
			Mg_conc[i][j] = 0.0;
			Fe_conc[i][j] = 0.0;
			Mg_conc_b[i][j] = 0.0;
			Fe_conc_b[i][j] = 0.0;
			Is_Reactive_Grain[i][j] = 0;
		}
	}
}

//-------------------------------------------------------------------------------------------------------------
// this function connects the particles of the lattice with the fluid lattice matrix
// the particle lattice has a repulsion box to track particles that is on the scale of the particles
// The fluid lattice matrix is larger, so one diffusion lattice grid point contains 4 boxes of the 
// repulsion box that stores the particles. The particles are arranged in a triangular grid, so there 
// may be more than one in the repulsion box. The repulsion box is a direct pointer to a particle that 
// is stored in there. That particle points to a potential second one in the box and so on. The end is 
// a NULL pointer. In order to talk forth and back between fluid lattice and lattice we need to find the 
// respective particles in the fluid box. 
// 
// the function is passed a grain number (flynn number from the interface), an index for what grain 
// this is supposed to be
// and the the box as Particle **list. This is the repulsion box stored in the lattice. Now fluid_lattice 
// can directly acces the particles through that box in this function 
// 
// if the i,j loop goes from 1 to dim-1 the boundaries are not included. Otherwise it goes from 0 to dim/1
// its important to remember that c and c++ loops and matrices/vectors start with 0! 
// 
//-------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::Set_Mineral_Fluid(int gr, int min, Particle **list)
{
	int i,j, particle_in_box, k; 
	
	for (i = 1; i < dim-1; i++) // loop in x = i and y = j except for boundaries
	{
		for (j = 1; j < dim-1; j++)
		{	
			
				//--------------------------------------------------------------------------------------------
				// Now define counters for all 4 repulsion boxes that we have to look into to define the 
				// solid fraction for the pressure node as a function of i and j. 
				// 
				// the repulsion box is a one-dimensional list starting in the lower left hand corner of the 
				// lattice with 0 and running towards the right with TWICE the lattice width (so for a 100 
				// lattice up to 199). The box in the second horizontal row above 0 is then 200 and so on.
                //
                // at the moment this routine is not using the boundaries because the boxes there are
                // either not completely full or are empty. There is already the try to double up full boxes
                // for the boundaries, but that does not work yet, needs further debugging
				//--------------------------------------------------------------------------------------------
				
					for (k = 0; k < 4; k++) // four repulsion boxes 
					{						
						
						if (k == 0)
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2); // lower left
						else if (k == 1)
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2); // lower right 
						else if (k == 2)
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // upper left 
						else if (k == 3)
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // upper right 
						
						
						//-----------------------------------------------------------
						// If the box is filled deal with the particle 
						//-----------------------------------------------------------
						
						if(list[box]) // check there is a particle there 
						{
							help = list[box]; // help now points at particle, help is a dummy pointer 
							
							if (help->grain == gr) // the particles have a parameter 'grain' that has the number 
							{
								if (min == 1) // garnet? 
								{
									Mineral_Diffusion[i][j] = 1e-11; // diffusion coefficient 
									Mg_conc[i][j] = 0.05; // starting Mg concentration in grain 
									Fe_conc[i][j] = 0.04; // starting Fe concentration in grain 
									Is_Reactive_Grain[i][j] = 1; // not used right now 
								}
								if (min == 2) // biotite? 
								{
									Mineral_Diffusion[i][j] = 3e-11;  // diffusion coefficient
									Mg_conc[i][j] = 0.07;  // starting Mg concentration in grain 
									Fe_conc[i][j] = 0.03;  // starting Fe concentration in grain
									Is_Reactive_Grain[i][j] = 1;  // not used right now 
								}
							}						
							
							//-----------------------------------------------------------------------
							// check if there is an additional particle in the repulsion box
							// the additional particle would be attached to the help particle with 
							// the next_inBox pointer. 
							//-----------------------------------------------------------------------
							
							while (help->next_inBox)  // now do the same stuff for additional particles in the box 
							{
								help = help->next_inBox;
								
								if (help->grain == gr)
								{
									if (min == 1)
									{
										Mineral_Diffusion[i][j] = 1e-11;
										Mg_conc[i][j] = 0.05;
										Fe_conc[i][j] = 0.04;
										Is_Reactive_Grain[i][j] = 1;
									}
									if (min == 2)
									{
										Mineral_Diffusion[i][j] = 3e-11;
										Mg_conc[i][j] = 0.07;
										Fe_conc[i][j] = 0.03;
										Is_Reactive_Grain[i][j] = 1;
									}
								}														
							}							
						}	
					}
						
		}
	}
}

//----------------------------------------------------------------------------------------------------------
//	Now do the same for the fluid and set the values in the lattice 
//
// Fluid_Boundary[i][j] = sets the diffusion coefficient for the fluid that potentially can be different 
// everywhere 
// Fluid[i][j] is just an int with 0 for no fluid and 1 for I am fluid 
//
//----------------------------------------------------------------------------------------------------------

void Fluid_Lattice::Set_Boundary_Fluid(double background, double bound, Particle **list)
{
	int i,j, particle_in_box, k; 
	
	for (i = 0; i < dim; i++) // loop in x = i and y = j except for boundaries
	{
		for (j = 0; j < dim; j++)
		{	
			
			Fluid_Boundary[i][j] = background; // diffusion coefficient of background 
			Fluid[i][j] = 0; // everything is NOT fluid at the beginning 
					
				//--------------------------------------------------------------------------------------------
				// Now define counters for all 16 repulsion boxes that we have to look into to define the 
				// solid fraction for the pressure node as a function of i and j. 
				//  
				//
				// the repulsion box is a one-dimensional list starting in the lower left hand corner of the 
				// lattice with 0 and running towards the right with TWICE the lattice width (so for a 100 
				// lattice up to 199). The box in the second horizontal row above 0 is then 200 and so on.
                //
                // at the moment this routine is not using the boundaries because the boxes there are
                // either not completely full or are empty. There is already the try to double up full boxes
                // for the boundaries, but that does not work yet, needs further debugging
				//--------------------------------------------------------------------------------------------
				
					for (k = 0; k < 4; k++) // again the four repulsion boxes in your fluid node 
					{						
						if (k == 0)
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2); // lower left
						else if (k == 1)
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2); // lower right 
						else if (k == 2)
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // upper left 
						else if (k == 3)
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // upper right 
						
						
						//-----------------------------------------------------------
						// If the box is filled deal with the particle 
						//-----------------------------------------------------------
						
						if(list[box]) // is there a particle in the box (there should be one..) 
						{
							help = list[box]; // help now points at particle 
							
							if (help->is_boundary == 1) // if the particle is_boundary is 1 its a grain boundary 
							{
								Fluid_Boundary[i][j] = bound; // diffusion coefficient for the boundary
								Fluid[i][j] = 1;   // 1 for it is a boundary box
								Mg_conc_b[i][j] = 0.04; // give it some initial concentration of Mg 
								Fe_conc_b[i][j] = 0.02; // give it some initial concentration of Fe 
							}						
							
							
							//-----------------------------------------------------------------------
							// check if there is an additional particle in the repulsion box
							// the additional particle would be attached to the help particle with 
							// the next_inBox pointer. 
							//-----------------------------------------------------------------------
							
							while (help->next_inBox)
							{
								// and do the same stuff as above. 
								help = help->next_inBox;
								
								if (help->is_boundary)
								{
									Fluid_Boundary[i][j] = bound;
									Fluid[i][j] = 1;
									Mg_conc_b[i][j] = 0.04;
									Fe_conc_b[i][j] = 0.02;
								}	
																			
								
							}							
						}	
					}
				
				
					
						
		}
	}
}



//------------------------------------------------------------------------------------------
// Passing back the values to the solid lattice , so the concentrations of Mg and Fe for 
// solid grains and the grain boundaries 
// 
// This takes each pressure node and looks into its 4 repulsion boxes and gives the values to 
// the particles in there.
//
// So talking back to the lattice and the particles from the fluid lattice
//-------------------------------------------------------------------------------------------

void Fluid_Lattice::Pass_Back_Concentration(Particle **list) //again gets the replusion box as direct pointer from the lattice  
{
	int i,j,k;
	Particle *help; // used as pointer to reach particles
	
	
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			//--------------------------------------------------------------------
			// 0 is lower left box, 1 lower right box, 2 upper left box
			// and 3 upper right box. 
			//
			// we need special boundary conditions for the boundary boxes
			// left and right can be wrappinp, but up and down may be more 
			// problematic
			//
			// at the moment right and left wrap, up and down copy next row
			//--------------------------------------------------------------------
			
			for (k = 0; k < 4; k++) // go to four quadrants of pressure node
			{
				if (k == 0)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
				else if (k == 1)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
				else if (k == 2)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
				else if (k == 3)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
				
				if(list[box])
				{
					help = list[box]; // set the pointer 
				
					help->Mg = Mg_conc[i][j];  // Mg in solid 
					
					help->Mg_fluid = Mg_conc_b[i][j]; // Mg in fluid 
				
					help->Fluid = Fluid_Boundary[i][j]; // visualize the grain boundaries or fluid 
					
					help->Fe = Fe_conc[i][j]; // Fe in solid 
					
					help->Fe_fluid = Fe_conc_b[i][j]; // Fe in fluid 
				
						
					while(help->next_inBox) // and do the whole thing for other particles in the box
					{						
						help = help->next_inBox; // and set pointer to next particle

						help->Mg = Mg_conc[i][j];  // Mg in solid 
					
						help->Mg_fluid = Mg_conc_b[i][j]; // Mg in fluid 
				
						help->Fluid = Fluid_Boundary[i][j]; // visualize the grain boundaries or fluid 
					
						help->Fe = Fe_conc[i][j]; // Fe in solid 
					
						help->Fe_fluid = Fe_conc_b[i][j]; // Fe in fluid 
				
					}				
				}
						
			}
		}
	}
	
}

//------------------------------------------------------------------------------
//
// solve the diffusion equation for the solid using the ADI mixed 
// implicit - explicit approach 
//
// needs to be done several times for as many elements we have 
// now two Mg and Fe 
//
//-------------------------------------------------------------------------------

void Fluid_Lattice::SolveDiffusionImplicitSolid(double time, double space)
{
	// Mg solve for Mg first 
	
	alpha_set_solid(time); // set the initial prefactor 
				
	matrices_set_1();  // set the first matrix for inversion 
	multiplication_solid_Mg(); // multiply concentration matrix with first matrix, explicit step
	transpose_trans_dif(); // transpose matrix
	invert();			   // invert the matrix 
	multiplication_inv_solid_Mg(); // multiply with inverted matrix, implicit step 
					
	matrices_set_2(); // matrix two changes x and y 
	multiplication_solid_Mg(); // again explicit term 
	transpose_trans_dif(); // transpose
	invert();  // invert the matrix 
	multiplication_inv_solid_Mg(); // multiply the inverted matrix, implicit step 
	
	// do the same stuff with Fe
	
	alpha_set_solid(time);
				
	matrices_set_1();
	multiplication_solid_Fe();
	transpose_trans_dif();
	invert();			
	multiplication_inv_solid_Fe();
					
	matrices_set_2();
	multiplication_solid_Fe();
	transpose_trans_dif();
	invert();
	multiplication_inv_solid_Fe();
}

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. 
//
// this difusion constant varies for different minerals 
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_solid( double time_dif) // need only alpha
{
	int i,j;
	double max;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;
			{
				alpha[i][j] = time_dif*Mineral_Diffusion[i][j]/ pow(width*scale_constt,2.0);
				if (alpha[i][j]>max) max = alpha[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}


//----------------------------------------------------------------------------
// multiply diagonal matrix with the concentration matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_solid_Mg()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Mg_conc[k][j];			
			}
		}
	}
}




//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_solid_Mg()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Mg_conc[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Mg_conc[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}

//----------------------------------------------------------------------------
// multiply diagonal matrix with the concentration matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_solid_Fe()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Fe_conc[k][j];			
			}
		}
	}
}




//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_solid_Fe()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Fe_conc[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Fe_conc[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}

//--------------------------------------------------------------------------------
// this is the old ADI method that is not used any more right now
// 
// Now the concentration in the fluid is calculated with the new explicit method
// 
//--------------------------------------------------------------------------------

void Fluid_Lattice::SolveDiffusionImplicitFluid(double time, double space)
{
	// Mg
	
	alpha_set_dif_por(time); // this was setting different coefficients for x and y 
	beta_set_dif_por(time);
				
	matrices_set_1();
	multiplication_fluid_Mg();
	transpose_trans_dif();
	invert();			
	multiplication_inv_fluid_Mg();
					
	matrices_set_2();
	multiplication_fluid_Mg();
	transpose_trans_dif();
	invert();
	multiplication_inv_fluid_Mg();
	
	// Fe
	
	alpha_set_dif_por(time);
	beta_set_dif_por(time);
				
	matrices_set_1_fluid();
	multiplication_fluid_Fe();
	transpose_trans_dif();
	invert();			
	multiplication_inv_fluid_Fe();
					
	matrices_set_2_fluid();
	multiplication_fluid_Fe();
	transpose_trans_dif();
	invert();
	multiplication_inv_fluid_Fe();
	
}

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation.
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::alpha_set_dif_por(double time_dif) // alpha is x
{
	int i,j;
	double max;
	double dif, dif1, dif2, dif3;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			alpha[i][j] = 0;
		}
	}
	for(i = 1; i < dim-1; i++)
	{
		for (j = 1; j < dim-1; j++)
		{
			{
				dif = (Fluid_Boundary[i][j]+Fluid_Boundary[i-1][j]+Fluid_Boundary[i+1][j])/3;
				dif1 = (Fluid_Boundary[i][j]+Fluid_Boundary[i-1][j])/2;
				dif2 = (Fluid_Boundary[i][j]+Fluid_Boundary[i+1][j])/2;
				dif3 = (Fluid_Boundary[i-1][j]+Fluid_Boundary[i+1][j])/2;
				
				if (dif == Fluid_Boundary[i][j])
					dif = Fluid_Boundary[i][j];
				else if (dif > Fluid_Boundary[i][j])
					dif = Fluid_Boundary[i][j];
				else if (dif1 == dif2)
					dif = Fluid_Boundary[i+1][j];
				else
					;
				
				
				alpha[i][j] = time_dif*dif/ pow(width*scale_constt,2.0);
				if (alpha[i][j]>max) max = alpha[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}

//--------------------------------------------------------------------------------------------------------------------
// This just fills the parameter matrix for the calculation. 
//--------------------------------------------------------------------------------------------------------------------

void Fluid_Lattice::beta_set_dif_por(double time_dif) // beta is y 
{
	int i,j;
	double max;
	double dif, dif1, dif2, dif3;
	
	max = 0;
	
	for(i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			beta[i][j] = 0;
		}
	}
	for(i = 1; i < dim-1; i++)
	{
		for (j = 1; j < dim-1; j++)
		{
			{
				dif = (Fluid_Boundary[i][j]+Fluid_Boundary[i][j-1]+Fluid_Boundary[i][j+1])/3;
				dif1 = (Fluid_Boundary[i][j]+Fluid_Boundary[i][j-1])/2;
				dif2 = (Fluid_Boundary[i][j]+Fluid_Boundary[i][j+1])/2;
				dif3 = (Fluid_Boundary[i][j-1]+Fluid_Boundary[i][j+1])/2;
				
				if (dif == Fluid_Boundary[i][j])
					dif = Fluid_Boundary[i][j];
				else if (dif > Fluid_Boundary[i][j])
					dif = Fluid_Boundary[i][j];
				else if (dif1 == dif2)
					dif = Fluid_Boundary[i][j+1];
				else
					;
					
					
					
				beta[i][j] = time_dif*dif/ pow(width*scale_constt,2.0);
				if (beta[i][j]>max) max = beta[i][j];										
			}
		} 
	}
	
	cout << "dif prefactor:" << max << endl;
}


//-----------------------------------------------------------------------------------
// set the matrix for the first calculation step
// this is the matrix that could be sparse potentially
//-----------------------------------------------------------------------------------


void Fluid_Lattice::matrices_set_1_fluid()
{	
  // creats L.H.S (al[][][])  and R.H.S (bl[][][])coeffiecient matrix 
  // at first half time step of ADI method
  
  int k,j,i;
  
  for (k=0; k<dim; k++)
  {                   
    for (i=0; i<dim; i++)
    {
		for (j=0;j<dim;j++)
		{
			al[k][i][j] = 0;      
			bl[k][i][j] = 0; 
        
			if (i==j)            //for diagonal elements
			{
				if (i==0 || i==dim-1)  //set boundary conditions
				{
					al[k][i][j] = 1.0;      //column set for any given column j				
					bl[k][i][j] = 1.0;      //row set for any given row i
				}
				else
				{
					al[k][i][j] = 1+2*alpha[k][i];				
					bl[k][i][j] = 1-2*beta[i][k];
				}
			}
			else if ((i==j-1 && i>0) || (i==j+1 && i<(dim-1)))
			{
				al[k][i][j] = -alpha[k][i];
			
				bl[k][i][j] = beta[i][k];
			}
		}
	 }
  }    	
}

//------------------------------------------------------------------------------------------
// Set the matrix for the second step of the calculation
//------------------------------------------------------------------------------------------

void Fluid_Lattice::matrices_set_2_fluid()
{	
  // creats L.H.S (al[][][])  and R.H.S (bl[][][])coeffiecient matrix 
  // at second half time step of ADI method
  
  int k,i,j;
  
  for (k=0;k<dim;k++)
  {
	for (i=0; i<dim; i++)
	{                   
		for (j=0; j<dim; j++)
		{
			al[k][i][j] = 0;      
			bl[k][i][j] = 0; 
        
			if (i==j)            //for diagonal elements
			{
				if (i==0 || i==dim-1)  //set boundary conditions
				{
					al[k][i][j] = 1.0;      //column set for any given column j					
					bl[k][i][j] = 1.0;      //row set for any given row i
				}
				else
				{
					al[k][i][j] = 1+2*beta[i][k];					
					bl[k][i][j] = 1-2*alpha[k][i];
				}
			}
			else if ((i==j-1 && i>0 ) || (i==j+1 && i<(dim-1)))
			{
				al[k][i][j] = -beta[i][k];
				
				bl[k][i][j] = alpha[k][i];
			}
		}
    }
  }
}


//----------------------------------------------------------------------------
// multiply diagonal matrix with the concentration matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_fluid_Mg()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Mg_conc_b[k][j];			
			}
		}
	}
}




//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_fluid_Mg()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Mg_conc_b[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Mg_conc_b[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}

//----------------------------------------------------------------------------
// multiply diagonal matrix with the concentration matrix
//-----------------------------------------------------------------------------

void Fluid_Lattice::multiplication_fluid_Fe()
{
	int i,j,k;
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			
			ddyc[i][j] = 0;  //ddyc is just used as interim here 
			
			for (k = 0; k < dim; k++)
			{
				ddyc[i][j] += bl[j][i][k] * Fe_conc_b[k][j];			
			}
		}
	}
}




//--------------------------------------------------------------------------------
// multiply interim matrix with inverted matrix 
//--------------------------------------------------------------------------------

void Fluid_Lattice::multiplication_inv_fluid_Fe()
{
	int i,j,k;
    
  for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Fe_conc_b[i][j] = 0;
			
			for (k = 0; k < dim; k++)
				Fe_conc_b[i][j] += a_invl[j][i][k] * ddyc[k][j];
				
		}
	}
}

//--------------------------------------------------------------------------------------------
// This is the new explict solver for the grain boundaries. It solves only in the boundaries. 
// It also takes care whether or not diffusion is one, 1.5 or two dimensional. 
//
//
//---------------------------------------------------------------------------------------------

void Fluid_Lattice::SolveDiffusionExplicitFluid(double time, double space)
{
	int i,j;
	double dif;
    
    // First lets do the Mg diffusion along the grain boundaries
	//
	// first define background and fix boundaries. Boundaries we may treat differently later on. 
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			ddyc[i][j] = 0; // set all to zero 
			
			if 	(Fluid[i][j]==1)
				ddyc[i][j] = 0.08; // define boundaries as two times background concentration (later divided by 2) 
		}
	}
	
	// loop through everything except the boundary (dim is size of lattice) 
	
	for (i = 1; i < dim-1; i++)
	{
		for(j = 1; j < dim-1; j++)
		{
			if (Fluid[i][j]==1) // this is a grain boundary with fluid
			{
				dif = Fluid_Boundary[i][j];  // diffusion coefficient for fluid. These are the same for Fe and Mg now, but we can easily change that
				
				// first do x which is i 
				
				if (Fluid[i+1][j]==0 && Fluid[i-1][j]==0) // both neighbours in x are not fluid
					ddyc[i][j] = Mg_conc_b[i][j];
				
				else if (Fluid[i+1][j]==0 && Fluid[i-1][j]==1)							
					ddyc[i][j] = Mg_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i-1][j]-Mg_conc_b[i][j]); // backwards a fluid and forward not
				
				else if (Fluid[i-1][j]==0 && Fluid[i+1][j]==1)
					ddyc[i][j] = Mg_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i+1][j]-Mg_conc_b[i][j]); // forward a fluid and backward not
	
				else if  (Fluid[i-1][j]==1 && Fluid[i+1][j]==1)
					ddyc[i][j] = Mg_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i-1][j]-2*Mg_conc_b[i][j]+ Mg_conc_b[i+1][j]); // normal case, both neighbours fluid
					
				else 
					cout << "someting is wrong x" << endl; // just in case
				
				// and now do y which is j
				
				if (Fluid[i][j+1]==0 && Fluid[i][j-1]==0)	// both neighbours in y are not fluid	
					ddyc[i][j] = Mg_conc_b[i][j]+ddyc[i][j];
					
				else if (Fluid[i][j+1]==0 && Fluid[i][j-1]==1)
					ddyc[i][j] = ddyc[i][j] +Mg_conc_b[i][j]+ (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i][j-1]-Mg_conc_b[i][j]); // backwards fluid and forward not
					
				else if (Fluid[i][j-1]==0 && Fluid[i][j+1]==1)				
					ddyc[i][j] = ddyc[i][j] +Mg_conc_b[i][j]+  (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i][j+1]-Mg_conc_b[i][j]); // forward fluid and backward not
						
				else if (Fluid[i][j-1]==1 && Fluid[i][j+1]==1)
					ddyc[i][j] = ddyc[i][j] +Mg_conc_b[i][j]+  (time*dif/ pow(width*scale_constt,2.0))*(Mg_conc_b[i][j-1]-2*Mg_conc_b[i][j]+ Mg_conc_b[i][j+1]); // normal case, both neighbours fluid
				
				else 
					cout << "someting is wrong y" << endl; // just in case 
			
			}
		}
	}
	
	// and go through the loop again to fill the original matrix
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Mg_conc_b[i][j] = 0.5*ddyc[i][j]; // fill the original matrix 
				
		}
	}

	
	
	// Now lets do the Fe diffusion along the grain boundaries
	//
	// first define background and fix boundaries. Boundaries we may treat differently later on. 
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			ddyc[i][j] = 0; // set all to zero 
			
			if 	(Fluid[i][j]==1)
				ddyc[i][j] = 0.04; // define boundaries as two times background concentration (later divided by 2) 
		}
	}
	
	// loop through everything except the boundary (dim is size of lattice) 
	
	for (i = 1; i < dim-1; i++)
	{
		for(j = 1; j < dim-1; j++)
		{
			if (Fluid[i][j]==1) // this is a grain boundary with fluid
			{
				dif = Fluid_Boundary[i][j];  // diffusion coefficient for fluid. These are the same for Fe and Mg now, but we can easily change that
				
				// first do x which is i 
				
				if (Fluid[i+1][j]==0 && Fluid[i-1][j]==0) // both neighbours in x are not fluid
					ddyc[i][j] = Fe_conc_b[i][j];
				
				else if (Fluid[i+1][j]==0 && Fluid[i-1][j]==1)							
					ddyc[i][j] = Fe_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i-1][j]-Fe_conc_b[i][j]); // backwards a fluid and forward not
				
				else if (Fluid[i-1][j]==0 && Fluid[i+1][j]==1)
					ddyc[i][j] = Fe_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i+1][j]-Fe_conc_b[i][j]); // forward a fluid and backward not
	
				else if  (Fluid[i-1][j]==1 && Fluid[i+1][j]==1)
					ddyc[i][j] = Fe_conc_b[i][j] + (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i-1][j]-2*Fe_conc_b[i][j]+ Fe_conc_b[i+1][j]); // normal case, both neighbours fluid
					
				else 
					cout << "someting is wrong x" << endl; // just in case
				
				// and now do y which is j
				
				if (Fluid[i][j+1]==0 && Fluid[i][j-1]==0)	// both neighbours in y are not fluid	
					ddyc[i][j] = Fe_conc_b[i][j]+ddyc[i][j];
					
				else if (Fluid[i][j+1]==0 && Fluid[i][j-1]==1)
					ddyc[i][j] = ddyc[i][j] +Fe_conc_b[i][j]+ (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i][j-1]-Fe_conc_b[i][j]); // backwards fluid and forward not
					
				else if (Fluid[i][j-1]==0 && Fluid[i][j+1]==1)				
					ddyc[i][j] = ddyc[i][j] +Fe_conc_b[i][j]+  (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i][j+1]-Fe_conc_b[i][j]); // forward fluid and backward not
						
				else if (Fluid[i][j-1]==1 && Fluid[i][j+1]==1)
					ddyc[i][j] = ddyc[i][j] +Fe_conc_b[i][j]+  (time*dif/ pow(width*scale_constt,2.0))*(Fe_conc_b[i][j-1]-2*Fe_conc_b[i][j]+ Fe_conc_b[i][j+1]); // normal case, both neighbours fluid
				
				else 
					cout << "someting is wrong y" << endl; // just in case 
			
			}
		}
	}
	
	// and go through the loop again to fill the original matrix
	
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			Fe_conc_b[i][j] = 0.5*ddyc[i][j]; // fill the original matrix 
				
		}
	}
}

// This is the exchange reaction that we have to work on, this is only a dummy at the moment

void Fluid_Lattice::Exchange(double time, double space, Particle **list) 
{
	int i,j,k, boundary;
	double factor;
	
	factor = 1;
    
	for (i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			boundary = 0;
		
			for (k = 0; k < 4; k++) // go to four quadrants of pressure node to find the particles in there
			{
				if (k == 0)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
				else if (k == 1)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
				else if (k == 2)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
				else if (k == 3)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
				
				
				
				if(list[box])
				{
					help = list[box];
				
					if (help->is_boundary)
					{
						if (help->grain == 107)
							boundary = 1;
						else if (help->grain == 30)
							boundary = 1;
					}	
					while(help->next_inBox) // and do the whole thing for other particles in the box
					{						
						help = help->next_inBox;

						if (help->is_boundary)
						{
							if (help->grain == 107)
								boundary = 1;
							else if (help->grain == 30)
								boundary = 1;
						}	
						
					}
					
				}
			}					
				
			if (boundary == 1)
			{
				
				Mg_conc_b[i][j] = Mg_conc_b[i][j] + 0.1 * Mg_conc[i][j];
				Mg_conc[i][j] = Mg_conc[i][j] - 0.1 * Mg_conc[i][j];
				
				Fe_conc_b[i][j] = Fe_conc_b[i][j] + 0.1 * Fe_conc[i][j];
				Fe_conc[i][j] = Fe_conc[i][j] - 0.1 * Fe_conc[i][j];
				
				
			}
				
				
				
		}
			
	}
}


void Fluid_Lattice::InitHeatSheet(int Temp)
{
	int i,j;
	
	for(i = 0; i < 200; i++) // x loop
	{
		for(j = 0; j < 100; j++)  // y loop
		{
			HeatSheet[i][j] = Temp;
			
			
		}
	}
}

void Fluid_Lattice::SetHeatSheetbox(double x, double x2, double y, double y2, int Temp)
{
	int i,j;
	
	for(i = 0; i < 200; i++) // x loop
	{
		for(j = 0; j < 100; j++)  // y loop
		{		
			if (i > x)
			{
				if (i < x2)
				{
					if (j > y)
					{
						if (j < y2)
						{
							HeatSheet[i][j] = Temp;		
						}
					}		
				}
			}
		}
	}
}

void Fluid_Lattice::SetHeatSheet(double pos, double pos2)
{
	int i,j;
	
	for(i = 0; i < 200; i++) // x loop
	{
		for(j = 0; j < 100; j++)  // y loop
		{
			HeatSheet[i][j] = 10;
			
			if (i > pos)
			{
				if (i < pos2)
				{
					HeatSheet[i][j] = 1000;				
				}
			}
		}
	}
}

void Fluid_Lattice::SolveHeatSheet(double space, double time, double dif) // time in years
{
	int i,j;
	double factor; 
	
	factor = time*60*60*24*350;
	factor = factor * dif /(space*space);
	
	if (factor > 0.45)
		cout << "this might crash temperature factor too high" << endl;
	
	for (i = 1; i < (200)-1; i++)
	{
		for(j = 1; j < dim-1; j++)
		{
			
			//IntHeatSheet[i][j] = HeatSheet[i][j] + (time*dif/ pow(space,2.0))*(HeatSheet[i-1][j]-2*HeatSheet[i][j]+ HeatSheet[i+1][j]);
			//IntHeatSheet[i][j] = IntHeatSheet[i][j] +HeatSheet[i][j]+  (time*dif/ pow(space,2.0))*(HeatSheet[i][j-1]-2*HeatSheet[i][j]+ HeatSheet[i][j+1]); 
			IntHeatSheet[i][j] = HeatSheet[i][j] + (factor)*(HeatSheet[i-1][j]-2*HeatSheet[i][j]+ HeatSheet[i+1][j]);
			IntHeatSheet[i][j] = IntHeatSheet[i][j] +HeatSheet[i][j]+  (factor)*(HeatSheet[i][j-1]-2*HeatSheet[i][j]+ HeatSheet[i][j+1]); 
		}
	}	
	
	for (i = 1; i < (200)-1; i++)
	{
		for(j = 1; j < dim-1; j++)
		{		
			HeatSheet[i][j] = 0.5*IntHeatSheet[i][j] - ((HeatSheet[i][j]-200)*1/100);
		}
	}
	for (i = 0; i < (200); i++)
	{
		HeatSheet[i][0] =  HeatSheet[i][2];
		HeatSheet[i][dim-1] =  HeatSheet[i][dim-2];
		HeatSheet[i][1] =  HeatSheet[i][2];
	}	
	for(j = 1; j < dim-1; j++)
	{		
		HeatSheet[0][j] = HeatSheet[1][j];
		HeatSheet[199][j] = HeatSheet[198][j];
	}
}

void Fluid_Lattice::Pass_Back_SheetT(Particle **list, double temp, double critAge) //again gets the replusion box as direct pointer from the lattice  
{
	int i,j,k;
	Particle *help; // used as pointer to reach particles
	bool flag;
	
	for (i = 0; i < dim*2; i++)
	{
		for (j = 0; j < dim; j++)
		{
			//--------------------------------------------------------------------
			// 0 is lower left box, 1 lower right box, 2 upper left box
			// and 3 upper right box. 
			//
			// we need special boundary conditions for the boundary boxes
			// left and right can be wrappinp, but up and down may be more 
			// problematic
			//
			// at the moment right and left wrap, up and down copy next row
			//--------------------------------------------------------------------
			
			flag = false;
			
			for (k = 0; k < 4; k++) // go to four quadrants of pressure node
			{
				if (k == 0)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
				else if (k == 1)
					box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
				else if (k == 2)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
				else if (k == 3)
					box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
				
				if(list[box])
				{
					help = list[box]; // set the pointer 
					
					if(help->age < critAge)
						flag = true;
					if (flag)
						HeatSheet[i][j] = 1200; //temp

					
					help->Tdif = help->SheetT - HeatSheet[i][j]; //other: sqrt((help->SheetT - HeatSheet[i][j])*(help->SheetT - HeatSheet[i][j]))
					
					
					help->age ++;
					help->SheetT = HeatSheet[i][j];  // Mg in solid 
					//if (help->Tdif < 0)
					//cout << help->Tdif << endl;
						
					while(help->next_inBox) // and do the whole thing for other particles in the box
					{						
						help = help->next_inBox; // and set pointer to next particle
						
						if(help->age < critAge)
							flag = true;
						if (flag)
							HeatSheet[i][j] = 1200; //temp
						
						help->Tdif = help->SheetT - HeatSheet[i][j]; //other: sqrt((help->SheetT - HeatSheet[i][j])*(help->SheetT - HeatSheet[i][j]))
						
						help->age ++;
						
						help->SheetT = HeatSheet[i][j];  // Mg in solid 
															
					}				
				}
						
			}
		}
	}
	
}


void Fluid_Lattice::Read_TSheet(Particle **list, int boundary)
{
	int i,j,k, particle_in_box, count;
	double solid_average, T;

// loop through the matrix, i is x, and j is y (note that Irfan did the opposite in pressure lattice)
// Each fluid box contains 4 repulsion boxes. 

// calculate an average for the boundary conditions. These boundary conditions turn out to be a major pain
// there are three different ones at the moment, the average is number 3
    
	
	count = 0;

	for (i = 1; i < (dim*2)-1; i++) // loop in x = i and y = j except for boundaries
	{
		for (j = 1; j < dim-1; j++)
		{	
			
			particle_in_box = 0;	// count sum of particles in the box (FLUID box)
			T = 0;				// solid fraction sum up
						
			
			//------------------------------------------------------------------------------------------------
			// In order to get the right solid fraction and to avoid grid effects converting a triangular
			// solid lattice into the square fluid lattice we use a smoothing function and consider solid
			// in the four repulsion boxes of the fluid node plus the surrounding 12 repulsion boxes.  				
			// The solid is then weight using the smoothing function depending on how far the particle is 				 
			// from the centre of the pressure node. 
			//
			// this is also applied to the velocities. 
			//-------------------------------------------------------------------------------------------------
			
			//-------------------------------------------------------------------------------------------------
			// Because of the above the outer rows of the matrix have to be treated separately, otherwise 
			// we look into boxes that dont exist. 
			//-------------------------------------------------------------------------------------------------
			
			//if((i > 0 && i < dim-1) && (j > 0 && j < dim-1))
			{		
				//--------------------------------------------------------------------------------------------
				// Now define counters for all 16 repulsion boxes that we have to look into to define the 
				// solid fraction for the pressure node as a function of i and j. 
				// start in the lower left hand corner with k = 0, then go to the right. So 0,1,2,3 are below 
				// the actual pressure node, 5,6,9,10 are in the actual pressure node. 
				//
				// the repulsion box is a one-dimensional list starting in the lower left hand corner of the 
				// lattice with 0 and running towards the right with TWICE the lattice width (so for a 100 
				// lattice up to 199). The box in the second horizontal row above 0 is then 200 and so on.
                //
                // at the moment this routine is not using the boundaries because the boxes there are
                // either not completely full or are empty. There is already the try to double up full boxes
                // for the boundaries, but that does not work yet, needs further debugging
				//--------------------------------------------------------------------------------------------
				
					for (k = 0; k < 16; k++) 
					{
						if (k == 0)
						{
							box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // pl_size = resolution of particle lattice
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2); // take 2
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size))+ 4*pl_size +1+((i-1)*2); // take 8
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10
							
								
						}
						else if (k == 1)
						{
							box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2);
							if (i==0)
								box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2); // take 3
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
							
								
						}
						else if (k == 2)
						{
							box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // take 0
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
							
								
						}
						else if (k == 3)
						{
							box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2); // take 1
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2); // take 11	
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							
						}
							
						else if (k == 4)
						{
							box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2); // take 6
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10	
					
							
						}
						else if (k == 5)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +4+((i-1)*2); // take 7
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2); // take 13
							if (j == 0 && i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10									
						}
						else if (k == 6)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2); // take 4
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
						
								
						}
						else if (k == 7)
						{
							box = (((j*2)-1)*(2*pl_size)) + 2*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							if (j == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2); // take 15
							if (j == 0 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9	
						
								
						}	
						else if (k == 8)
						{
							box = (((j*2)-1)*(2*pl_size))+ 4*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2); // take 10
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+1+((i-1)*2); // take 0
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
								
						}
						else if (k == 9)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2); // take 11
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+2+((i-1)*2); // take 1
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
								
						}
						else if (k == 10)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +1+((i-1)*2); // take 8
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+3+((i-1)*2); // take 2
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
							
								
						}
						else if (k == 11)
						{
							box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 4*pl_size +2+((i-1)*2); // take 9
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+4+((i-1)*2); // take 3
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
								
						}	
						else if (k == 12)
						{
							box = (((j*2)-1)*(2*pl_size))+ 6*pl_size +1+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2); // take 14
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +1+((i-1)*2); // take 4
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
														
								
						}
						else if (k == 13)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2);
							if (i == 0)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2); // take 15
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							if (j == dim-1 && i == 0)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6	
						
								
						}
						else if (k == 14)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +3+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +1+((i-1)*2); // take 12
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +3+((i-1)*2); // take 6
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5	
							
								
						}
						else if (k == 15)
						{
							box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +4+((i-1)*2);
							if (i == dim-1)
								box = (((j*2)-1)*(2*pl_size)) + 6*pl_size +2+((i-1)*2); // take 13
							if (j == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +4+((i-1)*2); // take 7
							if (j == dim-1 && i == dim-1)
								box = (((j*2)-1)*(2*pl_size))+ 2*pl_size +2+((i-1)*2); // take 5
							
								
						}
						
						//-----------------------------------------------------------
						// If the box is filled deal with the particle 
						//-----------------------------------------------------------
						
						if(list[box])
						{
							help = list[box]; // help now points at particle 
							
							
	
							T = T + help->SheetT;	
													
							// count particles
														
							particle_in_box ++;
							
							//-----------------------------------------------------------------------
							// check if there is an additional particle in the repulsion box
							// the additional particle would be attached to the help particle with 
							// the next_inBox pointer. 
							//-----------------------------------------------------------------------
							
							while (help->next_inBox)
							{
								// and do the same stuff as above. 
								
								T = T + help->SheetT;	
							
								
								particle_in_box ++;
																			
								help = help->next_inBox;
							}							
						}	
					}
				
				// calculate solid fraction
				if (particle_in_box > 0)
					HeatSheet[i][j] = T/particle_in_box;
					
					
			}			
		}
	}

	if (boundary == 1)
	{
		// boundary conditions, apply after matrix filled, 
		// condition 1: simply copy the next row from the matrix
		// to the boundary row on sides as well as at bottom and 
		// top. Works with random background noise. Becomes
		// problematic when Elle grains are used (boundaries then
		// seem to slow things down because they are mainly oriented 90 degrees
		// to the boundary. 
		// condition 2: copy neighbour row to boundary row for upper and lower boundary
		// and wrap rows for left and right boundary. Again can be problematic when 
		// grain boundaries are used, but does help avoid "doubling" effects during dynamic
		// permeability development
		// condition 3: take an average of the whole lattice and apply that to boundaries. 
		// Needs testing. 
		
		for(j = 1; j < dim-1; j++) //copy neighbour row
		{
			HeatSheet[0][j] = HeatSheet[1][j];
		
			HeatSheet[dim-1][j] = HeatSheet[dim-2][j];
			
		}
		
		for(i = 0; i < dim*2; i++) //copy neighbour row
		{
			HeatSheet[i][0] = HeatSheet[i][1];
		
			//HeatSheet[i][(dim*2)-1] = HeatSheet[i][(dim*2)-2];
			HeatSheet[i][(dim)-1] = HeatSheet[i][(dim)-2];
		}	
	}
	
}

