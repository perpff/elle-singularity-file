 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile$
 * Revision:  $Revision$
 * Date:      $Date$
 * Author:    $Author$
 *
 ******************************************************/
#ifndef _E_timefn_h
#define _E_timefn_h
#ifdef __cplusplus
extern "C" {
#endif
char *GetLocalTime(void);
long currenttime(void);
#ifdef __cplusplus
}
#endif
#endif
